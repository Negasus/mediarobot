const { mix } = require('laravel-mix');

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
    'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.css'
], 'public/css/fancybox.css');

mix.scripts([
    'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js'
], 'public/js/fancybox.js');

mix.scripts([
    'resources/assets/js/pages/media-index.js'
], 'public/js/pages/media-index.js');
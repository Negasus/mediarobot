<?php

//include_once 'web/auth.php';

Route::auth();

Route::group([
    'middleware' => ['auth'],
], function(){

    require 'web/dashboard.php';
    require 'web/tasks.php';
    require 'web/filters.php';
    require 'web/media.php';
    require 'web/publishers.php';
    require 'web/groups.php';
    require 'web/logs.php';

});

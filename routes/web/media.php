<?php

Route::get('media',
    ['uses' => 'MediaController@index', 'as' => 'media.index'])
    ->middleware(['need_group',]);

Route::post('media/update',
    ['uses' => 'MediaController@update', 'as' => 'media.update']);
//    ->middleware('can:update,App\\Models\\Media');

Route::post('media/{media}/update/text',
    ['uses' => 'MediaController@updateText', 'as' => 'media.update.text']);

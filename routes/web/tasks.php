<?php

Route::get('tasks',
    ['uses' => 'TasksController@index', 'as' => 'tasks.index'])
    ->middleware(['need_group',]);

Route::post('tasks',
    ['uses' => 'TasksController@store', 'as' => 'tasks.store'])
    ->middleware('can:create,App\\Models\\Task');

Route::post('tasks/{task}/run',
    ['uses' => 'TasksController@run', 'as' => 'tasks.run']);
//    ->middleware('can:create,App\\Models\\Task');

Route::get('tasks/{task}/edit',
    ['uses' => 'TasksController@edit', 'as' => 'tasks.edit'])
    ->middleware('can:update,task');

Route::match(['PUT','PATCH'], 'tasks/{task}',
    ['uses' => 'TasksController@update', 'as' => 'tasks.update'])
    ->middleware('can:update,task');

Route::delete('tasks/{task}',
    ['uses' => 'TasksController@destroy', 'as' => 'tasks.destroy'])
    ->middleware('can:delete,task');

Route::get('tasks/{parserShortName}/create',
    ['uses' => 'TasksController@create', 'as' => 'tasks.create'])
    ->middleware('can:create,App\\Models\\Task');


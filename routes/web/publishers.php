<?php

Route::get('publishers',
    ['uses' => 'PublisherController@index', 'as' => 'publishers.index'])
    ->middleware(['need_group',]);

Route::post('publishers',
    ['uses' => 'PublisherController@store', 'as' => 'publishers.store'])
    ->middleware('can:create,App\\Models\\Publisher');

Route::post('publishers/{publisher}/run',
    ['uses' => 'PublisherController@run', 'as' => 'publishers.run']);

Route::get('publishers/{publisher}/edit',
    ['uses' => 'PublisherController@edit', 'as' => 'publishers.edit'])
    ->middleware('can:update,publisher');

Route::get('publishers/{publisherShortName}/create',
    ['uses' => 'PublisherController@create', 'as' => 'publishers.create'])
    ->middleware('can:create,App\\Models\\Publisher');

Route::match(['PUT', 'PATCH'], 'publishers/{publisher}',
    ['uses' => 'PublisherController@update', 'as' => 'publishers.update'])
    ->middleware('can:update,publisher');

Route::delete('publishers/{publisher}',
    ['uses' => 'PublisherController@destroy', 'as' => 'publishers.destroy'])
    ->middleware('can:delete,publisher');

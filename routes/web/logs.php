<?php

Route::get('log/system',
    ['uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index', 'as' => 'log.system.index']);

Route::get('log',
    ['uses' => 'LogController@index', 'as' => 'log.index']);

Route::post('log/clear',
    ['uses' => 'LogController@clear', 'as' => 'log.clear']);

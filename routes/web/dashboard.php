<?php

Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'dashboard.index']);
Route::get('phpinfo', ['uses' => 'DashboardController@phpinfo', 'as' => 'dashboard.phpinfo']);

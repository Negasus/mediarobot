<?php

Route::get('filters',
    ['uses' => 'FiltersController@index', 'as' => 'filters.index']);

Route::post('filters',
    ['uses' => 'FiltersController@store', 'as' => 'filters.store'])
    ->middleware('can:create,App\\Models\\Filter');

Route::get('filters/{filterShortName}/create',
    ['uses' => 'FiltersController@create', 'as' => 'filters.create'])
    ->middleware('can:create,App\\Models\\Filter');

Route::delete('filters/{filter}',
    ['uses' => 'FiltersController@destroy', 'as' => 'filters.destroy'])
    ->middleware('can:delete,filter');

Route::get('filters/{filter}',
    ['uses' => 'FiltersController@edit', 'as' => 'filters.edit'])
    ->middleware('can:update,filter');

Route::match(['PUT', 'PATCH'],'filters/{filter}',
    ['uses' => 'FiltersController@update', 'as' => 'filters.update'])
    ->middleware('can:update,filter');


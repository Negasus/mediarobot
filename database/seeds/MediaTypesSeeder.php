<?php

use Illuminate\Database\Seeder;

class MediaTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\MediaType::create(['name' => 'Photo']);
        \App\Models\MediaType::create(['name' => 'Video']);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('media', function (Blueprint $table) {
            $table->increments('id');
            // Уникальный индентификатор медиа (каждый парсер строит по своему, но в начале namespaced parser name и нижнее подчеркивание)
            $table->string('_id')->unique();

            $table->unsignedInteger('group_id');
            $table->unsignedInteger('task_id');
            $table->unsignedInteger('type_id')->default(1); // Photo
            $table->string('parser_type');

            $table->string('title')->nullable();
            $table->longText('description')->nullable();
            $table->string('url')->nullable(); // Прямая ссылка на медиа
            $table->string('link')->nullable(); // ССылка на медиа, как часть источника
            $table->text('tags')->nullable(); // Теги (разделенные #)

            $table->unsignedInteger('status')->default(\App\Models\Media::STATUS_NONE);

            $table->dateTime('published_at')->nullable();
            $table->string('published_to')->nullable();

            $table->timestamps();

            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
            $table->foreign('type_id')->references('id')->on('media_types')->onDelete('cascade');
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('media');

        Schema::enableForeignKeyConstraints();
    }
}

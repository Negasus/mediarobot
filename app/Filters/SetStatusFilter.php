<?php

namespace App\Filters;

use App\Contracts\FilterContract;
use App\Models\Media;
use Illuminate\Validation\Rule;

class SetStatusFilter extends BaseFilter implements FilterContract
{

    /**
     * @inheritdoc
     */
    public function apply(Media $media)
    {
        $media->status = $this->filter->filter_params->status;

        return $media;
    }

    /**
     * @inheritdoc
     */
    public static function getDefaultParams()
    {
        return [
            'status' => Media::STATUS_NONE,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getValidationRules()
    {
        return [
            'filter_params.status' => [
                'required',
                Rule::in([Media::STATUS_APPROVED, Media::STATUS_BLOCKED, Media::STATUS_PUBLISHED]),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getValidationMessages()
    {
        return [
            'filter_params.status.required' => 'Поле "Статус" должно быть заполнено',
        ];
    }
}
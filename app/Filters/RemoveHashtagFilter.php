<?php

namespace App\Filters;

use App\Contracts\FilterContract;
use App\Models\Media;

class RemoveHashtagFilter extends BaseFilter implements FilterContract
{

    /**
     * Применить фильтр к медиа. Если возвращается false - медиа не сохранять
     *
     * @param Media $media
     * @return Media|bool
     */
    public function apply(Media $media)
    {
        $regex = '/#[\p{L}\p{N}]+/u';

        $media->description = preg_replace($regex, '', $media->description);

        return $media;
    }

    /**
     * @return array
     */
    public static function getDefaultParams()
    {
        return [];
    }

    /**
     * Правила валидации параметров фильтра
     *
     * @return array
     */
    public static function getValidationRules()
    {
        return [];
    }

    /**
     * Сообщение валидатора об ошибках
     *
     * @return array
     */
    public static function getValidationMessages()
    {
        return [];
    }
}
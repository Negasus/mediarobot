<?php

namespace App\Filters;

use App\Contracts\FilterContract;
use App\Models\Media;

class ChangeDescriptionFilter extends BaseFilter implements FilterContract
{

    /**
     * @inheritdoc
     */
    public function apply(Media $media)
    {
        $regex = $this->filter->filter_params->regex;


        \Log::info('REGEX process');
        \Log::info($regex);
        \Log::info($media->description);

        if (false === $result = preg_match($regex, $media->description)) {
            \Log::info('BAD REGEX');
            return false;
        }

//        $media->description = preg_replace($regex, $this->filter->filter_params->replacement, $media->description);
        $newDescription = preg_replace($regex, $this->filter->filter_params->replacement, $media->description);

        \Log::info('CHANGED');
        \Log::info($newDescription);

        return $media;
    }

    /**
     * @inheritdoc
     */
    public static function getDefaultParams()
    {
        return [
            'regex' => '',
            'replacement' => '',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getValidationRules()
    {
        return [
            'filter_params.regex' => 'required'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getValidationMessages()
    {
        return [
            'filter_params.regex.required' => 'Поле "Выражение" должно быть заполнено',
        ];
    }
}
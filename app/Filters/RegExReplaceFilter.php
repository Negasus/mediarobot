<?php

namespace App\Filters;

use App\Contracts\FilterContract;
use App\Models\Media;

class RegExReplaceFilter extends BaseFilter implements FilterContract
{

    /**
     * @inheritDoc
     */
    public function apply(Media $media)
    {
        $media->description = preg_replace($this->filter->filter_params->regex, $this->filter->filter_params->replace, $media->description);
        return $media;
    }

    /**
     * @inheritDoc
     */
    public static function getDefaultParams()
    {
        return [
            'regex' => '//u',
            'replace' => '',
        ];
    }

    /**
     * @inheritDoc
     */
    public static function getValidationRules()
    {
        return [
            'filter_params.regex' => 'required|max:255|regexpression',
            'filter_params.replace' => 'max:255'
        ];
    }

    /**
     * @inheritDoc
     */
    public static function getValidationMessages()
    {
        return [
            'filter_params.regex.regexpression' => 'Регулярное выражение имеет неверный формат',
        ];
    }
}
<?php

namespace App\Filters;

use App\Contracts\FilterContract;
use App\Helpers\Helper;
use App\Models\Media;

class SetRandomTitleFilter extends BaseFilter implements FilterContract
{
    /**
     * @inheritdoc
     */
    public function apply(Media $media)
    {
        $titles = Helper::textToArray($this->filter->filter_params->text);

        $media->title = $titles->random();

        return $media;
    }

    /**
     * @inheritdoc
     */
    public static function getDefaultParams()
    {
        return [
            'text' => '',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getValidationRules()
    {
        return [
            'filter_params.text' => 'required',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getValidationMessages()
    {
        return [
            'filter_params.text.required' => 'Поле "Варианты заголовков" должно быть заполнено',
        ];
    }
}
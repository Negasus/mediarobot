<?php

namespace App\Filters;

use App\Models\Filter;

class BaseFilter
{
    protected $filter;

    function __construct(Filter $filter)
    {
        $this->filter = $filter;
    }
}
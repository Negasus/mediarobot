<?php

namespace App\Filters;

use App\Contracts\FilterContract;
use App\Models\Media;

class MakeTitleFilter extends BaseFilter implements FilterContract
{
    private $type = [
        0 => 'processIfEmpty',
        1 => 'processIfSize',
        2 => 'process',
    ];

    /**
     * @inheritdoc
     */
    public function apply(Media $media)
    {
        $method = $this->type[$this->filter->filter_params->type];

        return $this->$method($media);
    }

    /**
     * @param Media $media
     * @return Media
     */
    private function processIfEmpty(Media $media)
    {
        return (mb_strlen($media->title) > 0) ? $media : $this->process($media);
    }

    /**
     * @param Media $media
     * @return Media
     */
    private function processIfSize(Media $media)
    {
        return (mb_strlen($media->title) > $this->filter->filter_params->size) ? $media : $this->process($media);
    }

    /**
     * @param Media $media
     * @return Media
     */
    private function process(Media $media)
    {
        $media->title = mb_substr($media->description, 0, $this->filter->filter_params->size);

        return $media;
    }

    /**
     * @inheritdoc
     */
    public static function getDefaultParams()
    {
        return [
            'size' => 10,
            'type' => 0,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getValidationRules()
    {
        return [
            'filter_params.size' => 'required|integer'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getValidationMessages()
    {
        return [
            'filter_params.size.required' => 'Поле "Количество символов" должно быть заполнено',
            'filter_params.size.integer' => 'Поле "Количество символов" должно быть числом',
        ];
    }
}
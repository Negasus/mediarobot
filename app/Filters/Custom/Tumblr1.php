<?php

namespace App\Filters\Custom;

use App\Contracts\FilterContract;
use App\Filters\BaseFilter;
use App\Models\Media;

class Tumblr1 extends BaseFilter implements FilterContract
{

    /**
     * Применить фильтр к медиа. Если возвращается false - медиа не сохранять
     *
     * @param Media $media
     * @return Media|bool
     */
    public function apply(Media $media)
    {
        // Удаляем полностью ссылки (меняем на пробелы)
        $media->description = preg_replace('/<a.+<\/a>/Uis', ' ', $media->description);
        // Удаляем все HTML теги (меняем на пробелы)
        $media->description = preg_replace('/<.+>/Uis', ' ', $media->description);
        // Удалим спец символоы
        $media->description = preg_replace('/:/Uis', ' ', $media->description);
        // Все сдвоенные пробелы - на один
        $media->description = preg_replace('/\s+/', ' ', $media->description);
        // Вырежем URL
        $media->description = preg_replace('/http.+\s/', ' ', $media->description);
        $media->description = preg_replace('/www.+\s/', ' ', $media->description);
        // Обрежем пробелы
        $media->description = trim($media->description, " \t\n\r\0\x0B\xc2\xa0");
        // Копируем описание в заголовок
        $media->title = mb_substr($media->description, 0, $this->filter->filter_params->title_size);
        // Если описание полностью скопировралось в заголовок - потрем описание
        if (mb_strlen($media->title) == mb_strlen($media->description)) {
            $media->description = '';
        }
        $media->title = trim($media->title, " \t\n\r\0\x0B\xc2\xa0");

        // Если заголовок достаточной длины, аппрувим
        if (mb_strlen($media->title) >= $this->filter->filter_params->approve_size) {
            $media->status = Media::STATUS_APPROVED;
        }

        return $media;
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
     */
    public static function getDefaultParams()
    {
        return [
            'title_size' => 0,
            'approve_size' => 0,
        ];
    }

    /**
     * Правила валидации параметров
     *
     * @return array
     */
    public static function getValidationRules()
    {
        return [
            'filter_params.title_size' => 'required|integer|min:0|max:191',
            'filter_params.approve_size' => 'required|integer|min:0|max:191',
        ];
    }

    /**
     * Сообщение валидатора об ошибках
     *
     * @return array
     */
    public static function getValidationMessages()
    {
        return [
            'filter_params.title_size.required' => 'Поле "Размер заголовка" должно быть установлено',
            'filter_params.title_size.integer' => 'Значение поля "Размер заголовка" должно быть числом',
            'filter_params.title_size.min' => 'Значение поля "Размер заголовка" должно быть не менее :min',
        ];
    }
}
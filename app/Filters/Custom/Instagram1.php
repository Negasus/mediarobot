<?php

namespace App\Filters\Custom;

use App\Contracts\FilterContract;
use App\Filters\BaseFilter;
use App\Models\Media;

/**
 * Class Instagram1
 * @package App\Filters\Custom
 */
class Instagram1 extends BaseFilter implements FilterContract
{

    /**
     * @param Media $media
     * @return Media|bool
     */
    public function apply(Media $media)
    {
        // Вырежем хештеги и занесем в спец поле
        preg_match_all('/#([\p{L}\p{N}]+)/u', $media->description, $res, PREG_PATTERN_ORDER);
        $media->tags = $res[1];
        $media->description = preg_replace('/#[\p{L}\p{N}]+/u', ' ', $media->description);
        // Удаляем полностью ссылки (меняем на пробелы)
        $media->description = preg_replace('/<a.+<\/a>/Uis', ' ', $media->description);
        // Удаляем все HTML теги (меняем на пробелы)
        $media->description = preg_replace('/<.+>/Uis', ' ', $media->description);
        // Удаляем все аккаунты (меняем на пробелы)
        $media->description = preg_replace('/@[\p{L}\p{N}]+/u', ' ', $media->description);
        // Удалим Follow (меняем на пробелы)
        $media->description = str_ireplace('follow', ' ', $media->description);
        // Удалим спец символоы
//        $media->description = preg_replace('/:/Uis', ' ', $media->description);
        // Все сдвоенные пробелы - на один
        $media->description = preg_replace('/\s+/', ' ', $media->description);
        // Обрежем пробелы
        $media->description = trim($media->description);
        // Копируем описание в заголовок
        $media->title = mb_substr($media->description, 0, $this->filter->filter_params->title_size);
        // Если описание полностью скопировалось в заголовок - потрем его
        if (mb_strlen($media->title) == mb_strlen($media->description)) {
            $media->description = '';
        }
        // Если заголовок достаточной длины, аппрувим
        if (mb_strlen($media->title) >= $this->filter->filter_params->approve_size) {
            $media->status = Media::STATUS_APPROVED;
        }

        return $media;
    }

    /**
     * @return array
     */
    public static function getDefaultParams()
    {
        return [
            'title_size' => 190,
            'approve_size' => 10,
        ];
    }

    /**
     * @return array
     */
    public static function getValidationRules()
    {
        return [
            'filter_params.title_size' => 'required|integer|min:0|max:191',
            'filter_params.approve_size' => 'required|integer|min:0|max:191',
        ];
    }

    /**
     * @return array
     */
    public static function getValidationMessages()
    {
        return [
            'filter_params.title_size.required' => 'Поле "Размер заголовка" должно быть установлено',
            'filter_params.title_size.integer' => 'Значение поля "Размер заголовка" должно быть числом',
            'filter_params.title_size.min' => 'Значение поля "Размер заголовка" должно быть не менее :min',
        ];
        // todo: validation messages for approve_size
    }
}
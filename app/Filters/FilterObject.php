<?php

namespace App\Filters;

class FilterObject
{
    /**
     * @var null
     */
    private $id;
    /**
     * @var null
     */
    private $class;
    /**
     * @var null
     */
    private $name;
    /**
     * @var null
     */
    private $description;

    /**
     * FilterObject constructor.
     * @param null $id
     * @param null $class
     * @param null $name
     * @param null $description
     */
    function __construct($id = null, $class = null, $name = null, $description = null)
    {
        $this->id = $id;
        $this->class = $class;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param null $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

}
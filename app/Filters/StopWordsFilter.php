<?php

namespace App\Filters;

use App\Contracts\FilterContract;
use App\Models\Media;

class StopWordsFilter extends BaseFilter implements FilterContract
{
    /**
     * @inheritdoc
     */
    public function apply(Media $media)
    {
        $wordsString = trim($this->filter->filter_params->words);

        if ($wordsString == '') {
            return $media;
        }

        $words = explode("\n", $wordsString);

        foreach ($words as $word) {
            if (false !== strpos($media->description, $word) || false !== strpos($media->title, $word)) {
                return false;
            }
        }

        return $media;
    }

    /**
     * @inheritdoc
     */
    public static function getDefaultParams()
    {
        return [
            'words' => '',
            'type' => 0,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getValidationRules()
    {
        return [
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getValidationMessages()
    {
        return [
        ];
    }

}
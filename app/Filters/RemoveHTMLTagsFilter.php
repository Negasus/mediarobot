<?php

namespace App\Filters;

use App\Contracts\FilterContract;
use App\Models\Media;

class RemoveHTMLTagsFilter extends BaseFilter implements FilterContract
{

    /**
     * Применить фильтр к медиа. Если возвращается false - медиа не сохранять
     *
     * @param Media $media
     * @return Media|bool
     */
    public function apply(Media $media)
    {
        $media->description = preg_replace('/<.+>/Uis', '', $media->description);

        return $media;
    }

    /**
     * Параметры по умолчанию
     *
     * @return array
     */
    public static function getDefaultParams()
    {
        return [];
    }

    /**
     * Правила валидации параметров
     *
     * @return array
     */
    public static function getValidationRules()
    {
        return [];
    }

    /**
     * Сообщение валидатора об ошибках
     *
     * @return array
     */
    public static function getValidationMessages()
    {
        return [];
    }
}
<?php

namespace App\Filters;

use App\Contracts\FilterContract;
use App\Models\Media;
use App\Repositories\Media\MediaRepositoryInterface;

class BlockNonUniqueTitleFilter extends BaseFilter implements FilterContract
{

    /**
     * @inheritDoc
     */
    public function apply(Media $media)
    {
        /** @var MediaRepositoryInterface $media */
        $mediaRepository = app(MediaRepositoryInterface::class);

        if ($media->title != '' && $mediaRepository->titleExist($media->title)) {
            $media->status = Media::STATUS_BLOCKED;
        }

        return $media;
    }

    /**
     * @inheritDoc
     */
    public static function getDefaultParams()
    {
        return [
            'days' => 0,
        ];
    }

    /**
     * @inheritDoc
     */
    public static function getValidationRules()
    {
        return [
            'filter_params.days' => 'required|int|min:0'
        ];
    }

    /**
     * @inheritDoc
     */
    public static function getValidationMessages()
    {
        return [];
    }
}
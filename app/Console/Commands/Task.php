<?php

namespace App\Console\Commands;

use App\Jobs\RunTask;
use App\Services\UserLog;
use Cron\CronExpression;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class Task extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tasks:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatch active tasks';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        // Выбираем активные задачи
        $tasks = \App\Models\Task::where('active', true)->get();

        $count = 0;
        $tasks->each(function(\App\Models\Task $task) use (&$count) {
            // Если задача имеет невалидный крон, переведем ее в неактивные
            if (!CronExpression::isValidExpression($task->cron)) {
                $task->update([
                    'active' => false,
                ]);

                UserLog::add($task->user, 'В задаче #' . $task->id. ' "' . $task->name. '" ошибка расписания. Задача деактивирована' , UserLog::LEVEL_ERROR);

                return;
            }

            // Если текущее время подходит для выполнения задачи, отправим задачу в очередь
            if (CronExpression::factory($task->cron)->isDue()) {
                $count++;
                dispatch(new RunTask($task));
            }
        });

        $this->info('Queued ' . $count . ' jobs');
    }
}

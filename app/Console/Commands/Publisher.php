<?php

namespace App\Console\Commands;

use App\Jobs\RunPublisher;
use App\Repositories\Publisher\PublisherRepositoryInterface;
use Cron\CronExpression;
use Illuminate\Console\Command;

class Publisher extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publisher:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run active publishers';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @param PublisherRepositoryInterface $publisherRepository
     */
    public function handle(PublisherRepositoryInterface $publisherRepository)
    {
        $publishers = $publisherRepository->active();

        $count = 0;
        $publishers->each(function(\App\Models\Publisher $publisher) use (&$count) {
            // Если текущее время подходит для выполнения публикатора, отправим публикатор в очередь
            if (CronExpression::factory($publisher->cron)->isDue()) {
                $count++;
                dispatch(new RunPublisher($publisher));
            }
        });

        $this->info('Queued ' . $count . ' jobs');
    }
}

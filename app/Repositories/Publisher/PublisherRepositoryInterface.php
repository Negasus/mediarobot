<?php

namespace App\Repositories\Publisher;

interface PublisherRepositoryInterface
{
    /**
     * Get active publishers
     *
     * @return mixed
     */
    public function active();

    /**
     * Get all publishers
     *
     * @return mixed
     */
    public function all();
}
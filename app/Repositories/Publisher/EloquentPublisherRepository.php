<?php

namespace App\Repositories\Publisher;

use App\Models\Publisher;

class EloquentPublisherRepository implements PublisherRepositoryInterface
{

    /**
     * @inheritdoc
     */
    public function active()
    {
        return Publisher::where('active', true)->get();
    }

    /**
     * @inheritdoc
     */
    public function all()
    {
        return Publisher::all();
    }
}
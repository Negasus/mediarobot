<?php

namespace App\Repositories\Task;

use App\Models\Task;
use App\Parsers\ParserValueObject;

class EloquentTaskRepository implements TaskRepositoryInterface
{

    /**
     * @inheritdoc
     */
    public function all()
    {
        return auth()->user()->tasks()->with('group')->get();
    }

    /**
     * @inheritdoc
     */
    public function getById($id)
    {
        return auth()->user()->tasks()->where('id', $id)->firstOrFail();
    }

    /**
     * @inheritdoc
     */
    public function new(ParserValueObject $parser)
    {
        return new Task([
            'cron' => '* * * * *',
            'parser_type' => $parser->class,
            'parser_params' => ($parser->class)::getDefaultParams(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function create($attributes)
    {
        return auth()->user()->tasks()->create($attributes);
    }

    /**
     * @inheritdoc
     */
    public function update($id, $attributes)
    {
        $task = $this->getById($id);

        return $task->update($attributes);
    }

    /**
     * @inheritdoc
     */
    public function destroy($id)
    {
        $task = $this->getById($id);

        return $task->delete();
    }
}
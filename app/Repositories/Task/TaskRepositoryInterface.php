<?php

namespace App\Repositories\Task;

use App\Models\Task;
use App\Parsers\ParserValueObject;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

interface TaskRepositoryInterface
{
    /**
     * Get tasks with relations
     *
     * @return Collection
     */
    public function all();

    /**
     * Get the task by ID
     *
     * @param int $id
     * @return Task
     * @throws ModelNotFoundException
     */
    public function getById($id);

    /**
     * Return new task instance
     *
     * @param ParserValueObject $parser
     * @return Task
     */
    public function new(ParserValueObject $parser);

    /**
     * Create a task
     *
     * @param array $attributes
     * @return Task
     */
    public function create($attributes);

    /**
     * Update the task
     *
     * @param int $id Task ID
     * @param array $attributes
     * @return Task
     * @throws ModelNotFoundException
     */
    public function update($id, $attributes);

    /**
     * Delete the task
     *
     * @param int $id
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function destroy($id);
}
<?php

namespace App\Repositories\Media;

use App\Models\Media;

class EloquentMediaRepository implements MediaRepositoryInterface
{

    /**
     * @inheritdoc
     */
    public function massUpdate(array $ids, array $attributes)
    {
        return Media::whereIn('id', $ids)->update($attributes);
    }

    /**
     * @inheritDoc
     */
    public function titleExist($title)
    {
        return !! Media::where('title', $title)->count();
    }


}
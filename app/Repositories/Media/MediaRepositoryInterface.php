<?php

namespace App\Repositories\Media;

interface MediaRepositoryInterface
{
    /**
     * Mass update media
     *
     * @param array $ids Array of IDs [1, 2, 5, ... ]
     * @param array $attributes ['name' => 'foo', 'type' => 'bar', ... ]
     * @return mixed
     */
    public function massUpdate(array $ids, array $attributes);

    /**
     *
     *
     * @param string $title
     * @return bool
     */
    public function titleExist($title);
}
<?php

namespace App\Repositories\Group;

use Illuminate\Support\Collection;

interface GroupRepositoryInterface
{
    /**
     * Get groups
     *
     * @return Collection
     */
    public function all();
}
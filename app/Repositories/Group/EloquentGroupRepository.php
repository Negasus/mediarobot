<?php

namespace App\Repositories\Group;

use Illuminate\Support\Collection;

class EloquentGroupRepository implements GroupRepositoryInterface
{

    /**
     * Get groups
     *
     * @return Collection
     */
    public function all()
    {
        return auth()->user()->groups;
    }
}
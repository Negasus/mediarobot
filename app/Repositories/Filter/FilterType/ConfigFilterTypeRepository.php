<?php

namespace App\Repositories\Filter\FilterType;

use App\Filters\FilterObject;

class ConfigFilterTypeRepository implements FilterTypeRepositoryInterface
{

    /**
     * @inheritdoc
     */
    public function all()
    {
        $_filters = config('filters');

        $filters = collect();

        foreach ($_filters as $filter) {
            $filters->push(new FilterObject(
                $filter['id'],
                $filter['class'],
                $filter['name'],
                $filter['description']
            ));
        }

        return $filters;
    }
}
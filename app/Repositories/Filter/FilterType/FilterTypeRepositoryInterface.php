<?php

namespace App\Repositories\Filter\FilterType;

use Illuminate\Support\Collection;

interface FilterTypeRepositoryInterface
{
    /**
     * Get all FilterType objects
     *
     * @return Collection Collection of FilterObject
     */
    public function all();
}
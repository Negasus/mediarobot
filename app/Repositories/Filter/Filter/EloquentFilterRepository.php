<?php

namespace App\Repositories\Filter\Filter;

class EloquentFilterRepository implements FilterRepositoryInterface
{

    /**
     * @inheritdoc
     */
    public function all()
    {
        return auth()->user()->filters;
    }

    /**
     * @inheritdoc
     */
    public function common()
    {
        return auth()->user()->filters()->where('task_id', null)->get();
    }

}
<?php

namespace App\Repositories\Filter\Filter;

interface FilterRepositoryInterface
{
    /**
     * Get all filters
     *
     * @return mixed
     */
    public function all();

    /**
     * Get common filters (with nulled task_id)
     *
     * @return mixed
     */
    public function common();
}
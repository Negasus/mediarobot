<?php

namespace App\Repositories\Parser;

use App\Parsers\ParserValueObject;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;

interface ParserRepositoryInterface
{
    /**
     * Get parsers
     *
     * @return Collection Collection of ParserValueObject
     */
    public function all();

    /**
     * Get the parser by ID field
     *
     * @param string $id
     * @return ParserValueObject
     * @throws ModelNotFoundException
     */
    public function getById($id);

    /**
     * Get the parser by class field
     *
     * @param string $class
     * @return ParserValueObject
     * @throws ModelNotFoundException
     */
    public function getByClass($class);
}
<?php

namespace App\Repositories\Parser;

use App\Parsers\ParserValueObject;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class ConfigParserRepository
 * @package App\Repositories\Parser
 */
class ConfigParserRepository implements ParserRepositoryInterface
{

    /**
     * @inheritdoc
     */
    public function all()
    {
        $_parsers = config('parsers');

        $parsers = collect();

        foreach ($_parsers as $parser) {
            $parsers->push(new ParserValueObject($parser['id'], $parser['class'], $parser['name'], $parser['description']));
        }

        return $parsers;
    }

    /**
     * @inheritdoc
     */
    public function getById($id)
    {
        return $this->getByFieldName('id', $id);
    }

    /**
     * @inheritdoc
     */
    public function getByClass($class)
    {
        return $this->getByFieldName('class', $class);
    }

    /**
     * @param $field
     * @param $value
     * @return ParserValueObject
     */
    private function getByFieldName($field, $value)
    {
        $parsers = $this->all();

        foreach ($parsers as $parser) {
            if ($parser->$field == $value) {
                return $parser;
            }
        }

        throw new ModelNotFoundException('Parser with field "' . $field . '" and value "' . $value . '" not found');
    }
}
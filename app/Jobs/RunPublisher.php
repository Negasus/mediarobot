<?php

namespace App\Jobs;

use App\Contracts\PublisherContract;
use App\Models\Publisher;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class RunPublisher implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Publisher
     */
    private $publisher;

    /**
     * Create a new job instance.
     * @param Publisher $publisher
     */
    public function __construct(Publisher $publisher)
    {
        //
        $this->publisher = $publisher;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /**
         * @var PublisherContract $publisher
         */
        $publisher = new $this->publisher->publisher_type($this->publisher);

        $publisher->run();
    }
}

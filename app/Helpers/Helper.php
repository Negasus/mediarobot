<?php

namespace App\Helpers;

use Illuminate\Support\Collection;

class Helper
{
    /**
     * Разбивает входящий текст по переносу строки, удаляет пустые значения и возвращает коллекцию строк
     *
     * @param string $text
     * @return Collection
     */
    public static function textToArray($text)
    {
        $items = explode("\n", $text);

        $items = array_map(function($item) {
            return trim($item);
        }, $items);

        $items = array_filter($items, function($item) {
            return $item != '';
        });

        return collect($items);
    }
}
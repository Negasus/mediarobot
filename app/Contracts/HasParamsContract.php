<?php

namespace App\Contracts;

interface HasParamsContract
{
    /**
     * Параметры по умолчанию
     *
     * @return array
     */
    public static function getDefaultParams();

    /**
     * Правила валидации параметров
     *
     * @return array
     */
    public static function getValidationRules();

    /**
     * Сообщение валидатора об ошибках
     *
     * @return array
     */
    public static function getValidationMessages();
}
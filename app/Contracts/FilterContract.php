<?php

namespace App\Contracts;

use App\Models\Media;

interface FilterContract extends HasParamsContract
{
    /**
     * Применить фильтр к медиа. Если возвращается false - медиа не сохранять
     *
     * @param Media $media
     * @return Media|bool
     */
    public function apply(Media $media);

}
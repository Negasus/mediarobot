<?php

namespace App\Contracts;

interface PublisherContract extends HasParamsContract
{
    /**
     * Запуск публикатора
     *
     * @return mixed
     */
    public function run();
}
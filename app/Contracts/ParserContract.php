<?php

namespace App\Contracts;

interface ParserContract extends HasParamsContract
{
    /**
     * Запуск парсера на обработку задачи
     *
     * @return mixed
     */
    public function run();

}
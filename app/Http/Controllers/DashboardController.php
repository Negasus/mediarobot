<?php

namespace App\Http\Controllers;

class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $currentJobsCount = \DB::table('jobs')->count();

        return view('index', [
            'currentJobsCount' => $currentJobsCount,
        ]);
    }

    public function phpinfo()
    {
        return view('admin.phpinfo');
    }
}

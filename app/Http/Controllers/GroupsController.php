<?php

namespace App\Http\Controllers;

use App\Http\Requests\GroupUpdateFormRequest;
use App\Models\Group;
use Illuminate\Http\Request;

/**
 * Class GroupsController
 * @package App\Http\Controllers
 */
class GroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = auth()->user()->groups;

        return view('groups.index', [
            'groups' => $groups,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $group = new Group([
            'user_id' => auth()->user()->id,
        ]);

        return view('groups.edit', [
            'group' => $group,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param GroupUpdateFormRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(GroupUpdateFormRequest $request)
    {
        $data = $request->all();

        auth()->user()->groups()->create($data);

        $request->session()->flash('flash.success', 'Группа создана');

        return redirect()->route('groups.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit(Group $group)
    {
        return view('groups.edit', [
            'group' => $group
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param GroupUpdateFormRequest|Request $request
     * @param  \App\Models\Group $group
     * @return \Illuminate\Http\Response
     */
    public function update(GroupUpdateFormRequest $request, Group $group)
    {
        $group->update($request->all());

        $request->session()->flash('flash.success', 'Сохранено');

        return redirect()->route('groups.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  \App\Models\Group $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Group $group)
    {
        try {
            $group->delete();
        } catch (\Exception $exception) {
            $request->session()->flash('flash.warning', 'Ошибка удаления группы #' . $exception->getCode() . '! Возможно есть задачи или публикаторы, связанные с этой группой');

            return redirect()->route('groups.index');
        }

        $request->session()->flash('flash.error', 'Группа удалена');

        return redirect()->route('groups.index');
    }
}

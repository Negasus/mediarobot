<?php

namespace App\Http\Controllers;

use App\Http\Requests\PublisherUpdateFormRequest;
use App\Jobs\RunPublisher;
use App\Models\Publisher;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class PublisherController
 * @package App\Http\Controllers
 */
class PublisherController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $publishers = auth()->user()->publishers()->with('group')->get();

        $publisherTypes = config('publishers');

        return view('publishers.index', [
            'publishers' => $publishers,
            'publisherTypes' => $publisherTypes,
        ]);
    }

    /**
     * @param Publisher $publisher
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Publisher $publisher)
    {
        $groups = auth()->user()->groups;

        $publisherConfig = $this->getPublisherFromConfig($publisher->publisher_type, 'class');

        return view('publishers.edit', [
            'publisher' => $publisher,
            'groups' => $groups,
            'publisherShortName' => $publisherConfig['id'],
        ]);
    }

    /**
     * @param $publisherShortName
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create($publisherShortName)
    {
        $publisherConfig = $this->getPublisherFromConfig($publisherShortName);

        $groups = auth()->user()->groups;

        $publisher = new Publisher([
            'cron' => '*/5 * * * *',
            'publisher_type' => $publisherConfig['class'],
            'publisher_params' => $publisherConfig['class']::getDefaultParams(),
        ]);

        return view('publishers.edit', [
            'publisher' => $publisher,
            'groups' => $groups,
            'publisherShortName' => $publisherConfig['id'],
        ]);
    }

    /**
     * @param PublisherUpdateFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PublisherUpdateFormRequest $request)
    {
        $data = $request->all();

        $data['active'] = !! $request->get('active');

        auth()->user()->publishers()->create($data);

        $request->session()->flash('flash.success', 'Публикатор создан');

        return redirect()->route('publishers.index');
    }

    /**
     * @param $publisherShortName
     * @param string $field
     * @return mixed
     */
    private function getPublisherFromConfig($publisherShortName, $field = 'id')
    {
        $config = config('publishers');
        foreach ($config as $item) {
            if ($item[$field] == $publisherShortName) {
                return $item;
            }
        }

        throw new BadRequestHttpException('Publisher ' . $publisherShortName . ' not found');
    }

    /**
     * @param PublisherUpdateFormRequest $request
     * @param Publisher $publisher
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(PublisherUpdateFormRequest $request, Publisher $publisher)
    {
        $data = $request->all();

        $data['active'] = !! $request->get('active');

        $publisher->update($data);

        $request->session()->flash('flash.success', 'Сохранено');

        return redirect()->route('publishers.index');
    }

    /**
     * @param Request $request
     * @param Publisher $publisher
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, Publisher $publisher)
    {
        $publisher->delete();

        $request->session()->flash('flash.error', 'Публикатор удален');

        return redirect()->route('publishers.index');
    }

    /**
     * @param Request $request
     * @param Publisher $publisher
     * @return \Illuminate\Http\RedirectResponse
     */
    public function run(Request $request, Publisher $publisher)
    {
        $this->dispatch(new RunPublisher($publisher));

        $request->session()->flash('flash.info', 'Публикатор поставлен в очередь на выполнение');

        return redirect()->route('publishers.index');
    }
}

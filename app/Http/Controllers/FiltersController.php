<?php

namespace App\Http\Controllers;

use App\Http\Requests\FilterUpdateFormRequest;
use App\Models\Filter;
use App\Models\Task;
use Illuminate\Http\Request;

/**
 * Class FiltersController
 * @package App\Http\Controllers
 */
class FiltersController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $task = $this->getTaskFromRequest();

        $filtersList = config('filters');

        $filters = ($task) ? $task->filters() : Filter::where('task_id', null);

        $filters = $filters->orderBy('order', 'asc')->get();

        return view('filters.index', [
            'task' => $task,
            'filters' => $filters,
            'filtersList' => $filtersList,
        ]);
    }

    /**
     * @param $filterShortName
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create($filterShortName)
    {
        $filterConfig = $this->findFilterInConfig($filterShortName, 'id');

        $task = $this->getTaskFromRequest();

        $filter = new Filter([
            'name' => $filterConfig['name'],
            'task' => $task,
            'order' => 0,
            'filter_type' => $filterConfig['class'],
            'filter_params' => $filterConfig['class']::getDefaultParams(),
        ]);

        return view('filters.edit', [
            'filter' => $filter,
            'task' => $task,
            'filterShortName' => $filterConfig['id'],
        ]);
    }

    /**
     * @param FilterUpdateFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(FilterUpdateFormRequest $request)
    {
        auth()->user()->filters()->create($request->all());

        $request->session()->flash('flash.success', 'Фильтр создан');

        return redirect()->route('filters.index');
    }

    /**
     * @param Filter $filter
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Filter $filter)
    {
        $filterConfig = $this->findFilterInConfig($filter->filter_type, 'class');

        return view('filters.edit', [
            'filter' => $filter,
            'filterShortName' => $filterConfig['id'],
        ]);
    }

    /**
     * @param $value
     * @param string $field
     * @return mixed
     * @throws \Exception
     */
    private function findFilterInConfig($value, $field = 'class')
    {
        foreach (config('filters') as $item) {
            if ($item[$field] == $value) {
                return $item;
            }
        }

        throw new \Exception('Filter not found');
    }

    /**
     * @param Request $request
     * @param Filter $filter
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, Filter $filter)
    {
        $filter->delete();

        $request->session()->flash('flash.error', 'Фильтр удален');

        return redirect()->route('filters.index');
    }

    /**
     * @param FilterUpdateFormRequest $request
     * @param Filter $filter
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(FilterUpdateFormRequest $request, Filter $filter)
    {
        $filter->update($request->all());

        $request->session()->flash('flash.success', 'Сохранено');

        return redirect()->route('filters.index');
    }

    /**
     * @return null|Task
     */
    private function getTaskFromRequest()
    {
        return (request()->get('task')) ? $task = auth()->user()->tasks()->findOrFail(request()->get('task')) : null;
    }

}

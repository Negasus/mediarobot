<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * Class LogController
 * @package App\Http\Controllers
 */
class LogController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $log = auth()->user()->history()->latest()->get();

        return view('logs.index', [
            'history' => $log,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function clear(Request $request)
    {
        auth()->user()->history()->delete();

        $request->session()->flash('flash.success', 'Лог очищен');

        return redirect()->route('log.index');
    }
}

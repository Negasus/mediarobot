<?php

namespace App\Http\Controllers;

use App\Http\Requests\Task\TaskStoreFormRequest;
use App\Http\Requests\Task\TaskUpdateFormRequest;
use App\Jobs\RunTask;
use App\Models\Task;
use App\Repositories\Group\GroupRepositoryInterface;
use App\Repositories\Parser\ParserRepositoryInterface;
use App\Repositories\Task\TaskRepositoryInterface;

/**
 * Class TasksController
 * @package App\Http\Controllers
 */
class TasksController extends Controller
{

    /**
     * @var TaskRepositoryInterface
     */
    private $taskRepository;

    /**
     * @var ParserRepositoryInterface
     */
    private $parserRepository;

    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepository;

    /**
     * TasksController constructor.
     *
     * @param TaskRepositoryInterface $taskRepository
     * @param ParserRepositoryInterface $parserRepository
     * @param GroupRepositoryInterface $groupRepository
     */
    function __construct(
        TaskRepositoryInterface $taskRepository,
        ParserRepositoryInterface $parserRepository,
        GroupRepositoryInterface $groupRepository
    )
    {
        $this->taskRepository = $taskRepository;
        $this->parserRepository = $parserRepository;
        $this->groupRepository = $groupRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tasks.index', [
            'tasks' => $this->taskRepository->all(),
            'parsers' => $this->parserRepository->all(),
        ]);
    }

    /**
     * Форма для создания новой задачи
     *
     * @param string $parserId
     * @return \Illuminate\Http\Response
     */
    public function create($parserId)
    {
        $parser = $this->parserRepository->getById($parserId);

        return view('tasks.edit', [
            'task' => $this->taskRepository->new($parser),
            'groups' => $this->groupRepository->all(),
            'parser' => $parser,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TaskStoreFormRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskStoreFormRequest $request)
    {
        $data = $request->all();

        // todo: вынести установку bool полей
        $data['active'] = $request->has('active');

        $this->taskRepository->create($data);

        session()->flash('flash.success', 'Задача создана');

        return redirect()->route('tasks.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        return view('tasks.edit', [
            'task' => $task,
            'parser' => $this->parserRepository->getByClass($task->parser_type),
            'groups' => $this->groupRepository->all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TaskUpdateFormRequest $request
     * @param  \App\Models\Task $task
     * @return \Illuminate\Http\Response
     */
    public function update(TaskUpdateFormRequest $request, Task $task)
    {
        $data = $request->all();

        $data['active'] = $request->has('active');

        $this->taskRepository->update($task->id, $data);

        session()->flash('flash.success', 'Сохранено');

        return redirect()->route('tasks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $this->taskRepository->destroy($task->id);

        session()->flash('flash.error', 'Задача удалена');

        return redirect()->route('tasks.index');
    }

    /**
     * Put the RunTask job in the queue
     *
     * @param Task $task
     * @return \Illuminate\Http\RedirectResponse
     */
    public function run(Task $task)
    {
        $this->dispatch(new RunTask($task));

        session()->flash('flash.info', 'Задача добавлена в очередь на выполнение');

        return redirect()->route('tasks.index');
    }
}

<?php

namespace App\Http\Controllers;

use App\Contracts\FilterContract;
use App\Models\Filter;
use App\Models\Media;
use App\Models\User;
use App\Repositories\Filter\Filter\FilterRepositoryInterface;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

/**
 * Class MediaController
 * @package App\Http\Controllers
 */
class MediaController extends Controller
{

    /**
     * @var FilterRepositoryInterface
     */
    private $filterRepository;

    /**
     * MediaController constructor.
     * @param FilterRepositoryInterface $filterRepository
     */
    function __construct(FilterRepositoryInterface $filterRepository)
    {
        $this->filterRepository = $filterRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /**
         * @var User $user
         */
        $user = auth()->user();

        // Номер текущей группы
        $currentGroup = ($request->get('group')) ?: 0;

        if ($currentGroup == 0) {
            // Если номер равен нулю, то тебе медиа из всех групп пользователя
            $query = Media::whereIn('group_id', $user->groups()->select(['id'])->pluck('id'));
        } else {
            $query = $user->groups()->where('id', $currentGroup)->first()->media();
        }

        if ($filters = $request->get('filters')) {
            $filters = explode('|', $filters);

            $statuses = [];
            foreach ($filters as $filter) {
                if ($filter == 'new') {
                    $statuses[] = Media::STATUS_NONE;
                }
                if ($filter == 'approved') {
                    $statuses[] = Media::STATUS_APPROVED;
                }
                if ($filter == 'blocked') {
                    $statuses[] = Media::STATUS_BLOCKED;
                }
                if ($filter == 'published') {
                    $statuses[] = Media::STATUS_PUBLISHED;
                }
            }

            $query->whereIn('status', $statuses);
        }

        $count = $request->has('count') ? $request->get('count') : 20;

        $media = $query->with('task')->with('group')->latest()->paginate($count);

        return view('media.index', [
            'media' => $media,
            'groups' => $user->groups,
            'filters' => $this->filterRepository->common(),
            'count' => $count,
            'currentGroup' => $currentGroup,
        ]);
    }

    /**
     * Update the media collection
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $method = 'action' . ucfirst(strtolower($request->get('action')));

        $ids = array_keys($request->get('check'));

        if (substr($method, 0, 12) == 'actionFilter') {
            return $this->applyFilter($ids);
        }

        if (! method_exists($this, $method)) {
            throw new MethodNotAllowedHttpException([], 'Action ' . $method . ' not found');
        }

        $this->$method($ids);

        $request->session()->flash('flash.success', 'Сохранено');

        return redirect()->route('media.index', \GuzzleHttp\Psr7\parse_query($request->get('search')));
    }

    /**
     * @param $ids
     * @return \Illuminate\Http\RedirectResponse
     */
    private function applyFilter($ids)
    {
        $filterId = substr(request()->get('action'), strpos(request()->get('action'), ':') + 1);
        /**
         * @var Filter $filterModel
         */
        $filterModel = Filter::findOrFail($filterId);
        /**
         * @var FilterContract $filter
         */
        $filter = new $filterModel->filter_type($filterModel);

        foreach ($ids as $id) {
            /**
             * @var Media $media
             */
            $media = Media::findOrFail($id);
            $media = $filter->apply($media);
            $media->save();
        }

        request()->session()->flash('flash.success', 'Фильтр применен');

        return redirect()->route('media.index', \GuzzleHttp\Psr7\parse_query(request()->get('search')));
    }

    /**
     * @param Request $request
     * @param Media $media
     * @return Media
     */
    public function updateText(Request $request, Media $media)
    {
        $media->title = $request->get('title');
        $media->description = $request->get('description');
        $media->save();

        return $media;
    }

    /**
     * @param $ids
     */
    private function actionApprove($ids)
    {
        Media::whereIn('id', $ids)->update([
            'status' => Media::STATUS_APPROVED,
        ]);
    }

    /**
     * @param $ids
     */
    private function actionBlock($ids)
    {
        Media::whereIn('id', $ids)->update([
            'status' => Media::STATUS_BLOCKED,
        ]);
    }

    /**
     * @param $ids
     */
    private function actionDelete($ids)
    {
        // todo: Авторизация удаления каждого медиа
        Media::whereIn('id', $ids)->delete();
    }

    /**
     * @param $ids
     */
    private function actionUnmark($ids)
    {
        Media::whereIn('id', $ids)->update([
            'status' => Media::STATUS_NONE,
        ]);
    }
}

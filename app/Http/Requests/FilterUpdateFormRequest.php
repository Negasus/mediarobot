<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilterUpdateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $filter =$this->request->get('filter_type');
        $filterRules = $filter::getValidationRules();

        return array_merge([
            'name' => 'required|max:255',
            'order' => 'required|integer'
        ], $filterRules);
    }

    /**
     * Get the validation messages
     *
     * @return array
     */
    public function messages()
    {
        $filter =$this->request->get('filter_type');
        $filterMessages = $filter::getValidationMessages();

        return array_merge([
            'name.required' => 'Поле "Имя" должно быть заполнено',
            'name.max' => 'Поле "Имя" должно иметь длину не более 255 симоволов',
            'order.required' => 'Поле "Порядок выполнения" должно быть заполнено',
            'order.integer' => 'Поле "Порядок выполнения" должно быть числом',
        ], $filterMessages);
    }

}

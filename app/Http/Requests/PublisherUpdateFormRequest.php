<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PublisherUpdateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $publisher = $this->request->get('publisher_type');
        $publisherRules = $publisher::getValidationRules();

        return array_merge([
            'name' => 'required|max:255',
            'cron' => 'required|cron',
        ], $publisherRules);
    }

    /**
     * Get the validation messages
     *
     * @return array
     */
    public function messages()
    {
        $publisher = $this->request->get('publisher_type');
        $publisherMessages = $publisher::getValidationMessages();

        return array_merge([
            'name.required' => 'Поле "Имя" должно быть заполнено',
            'name.max' => 'Поле "Имя" должно иметь длину не более 255 симоволов',
            'cron.required' => 'Поле "Расписание" должно быть заполнено',
            'cron.cron' => 'Поле "Расписание" имеет неправильный формат',
        ], $publisherMessages);
    }

}

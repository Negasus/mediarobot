<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

class TaskStoreFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; // todo: .
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // todo: Рефакторинг. Вынести в родителя объединение правил?
        $parserType = $this->request->get('parser_type');
        $parserRules = $parserType::getValidationRules();

        return array_merge([
            'name' => 'required|max:255',
            'group_id' => 'required|integer|exists:groups,id',
            'cron' => 'required|cron',
        ], $parserRules);
    }

    /**
     * Get the validation messages
     *
     * @return array
     */
    public function messages()
    {
        // todo: Рефакторинг. Вынести в родителя объединение правил?
        $parserType = $this->request->get('parser_type');
        $parserMessages = $parserType::getValidationMessages();

        return array_merge([
            'name.required' => 'Поле "Имя" должно быть заполнено',
            'name.max' => 'Поле "Имя" должно иметь длину не более 255 симоволов',
            'group_id.required' => 'Поле "Группа" должно быть заполнено',
            'group_id.integer' => 'Поле "Группа" должно быть числом',
            'group_id.exists' => '"Группа" не существует',
            'cron.required' => 'Поле "Расписание" должно быть заполнено',
            'cron.cron' => 'Поле "Расписание" имеет неправильный формат',
        ], $parserMessages);
    }

}

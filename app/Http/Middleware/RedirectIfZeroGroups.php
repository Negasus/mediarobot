<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfZeroGroups
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->groups()->count() == 0) {

            session()->flash('flash.warning', \Lang::get('flash.create_one_group'));

            return redirect()->route('groups.index');
        }

        return $next($request);
    }
}

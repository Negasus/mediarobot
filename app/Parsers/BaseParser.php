<?php

namespace App\Parsers;

use App\Contracts\FilterContract;
use App\Models\Filter;
use App\Models\Media;
use App\Models\Task;

/**
 * Class BaseParser
 * @package App\Parsers
 */
class BaseParser
{
    /**
     * @var Task
     */
    protected $task;

    /**
     * BaseParser constructor.
     * @param Task $task
     */
    function __construct(Task $task)
    {
        $this->task = $task;
    }

    /**
     * @param Media $media
     * @return bool
     */
    protected function store(Media $media)
    {
        // Если медиа с таким ID существуем, выходим
        if (Media::where('_id', $media->_id)->first()) {
            return false;
        }

        $media->task_id = $this->task->id;

        // Применим фильтры
        if (! $media = $this->filtersApply($media)) {
            // Если фильтр вернул false, значит надо отклонить медиа
            return false;
        }

        $media->save();

        return true;
    }

    /**
     * @param Media $media
     * @return Media|bool
     */
    protected function filtersApply(Media $media)
    {
        foreach ($this->task->filters()->orderBy('order', 'asc')->get() as $filter) {
            /**
             * @var Filter $filter
             * @var FilterContract $filterInstance
             */
            $filterInstance = new $filter->filter_type($filter);

            if (!$media = $filterInstance->apply($media)) {
                return false;
            }
        }

        return $media;
    }

}
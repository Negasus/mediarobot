<?php

namespace App\Parsers\Instagram\Api;

use GuzzleHttp\Client;

/**
 * Class InstagramApi
 * @package App\Parsers\Instagram\Api
 */
class InstagramApi
{

    /**
     * @var Client
     */
    private $client;

    /**
     * InstagramApi constructor.
     */
    function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param $tag
     * @return \Illuminate\Support\Collection
     */
    public function getTopMediaByTag($tag)
    {
        $url = 'https://www.instagram.com/explore/tags/' . urlencode($tag) . '/?__a=1';

        $response = $this->client->get($url);

        $s = json_decode($response->getBody()->getContents(), true);

        return collect($s['tag']['top_posts']['nodes']);
    }

    /**
     * @param $tag
     * @return \Illuminate\Support\Collection
     */
    public function getRecentMediaByTag($tag)
    {
        $url = 'https://www.instagram.com/explore/tags/' . urlencode($tag) . '/?__a=1';

        $response = $this->client->get($url);

        $s = json_decode($response->getBody()->getContents(), true);

        return collect($s['tag']['media']['nodes']);
    }

    /**
     * @param $account
     * @return \Illuminate\Support\Collection
     */
    public function getRecentMediaFromAccount($account)
    {
        $url = 'https://www.instagram.com/' . urlencode($account) . '/?__a=1';

        $response = $this->client->get($url);

        $s = json_decode($response->getBody()->getContents(), true);

        return collect($s['user']['media']['nodes']);
    }


}
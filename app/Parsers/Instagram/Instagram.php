<?php

namespace App\Parsers\Instagram;

use App\Helpers\Helper;
use App\Models\Media;
use App\Contracts\ParserContract;
use App\Parsers\BaseParser;
use App\Parsers\Instagram\Api\InstagramApi;
use App\Services\UserLog;
use Illuminate\Support\Collection;

/**
 * Class Instagram
 * @package App\Parsers\Instagram
 */
class Instagram extends BaseParser implements ParserContract
{

    /**
     * @var array
     */
    private $types = [
        0 => 'getTopMediaByTag',
        1 => 'getRecentMediaByTag',
        2 => 'getRecentMediaFromAccount',
    ];

    /**
     * Запуск парсера на обработку задачи
     *
     * @return mixed|void
     */
    public function run()
    {
        $api = new InstagramApi();

        // Метод для обработки итемов
        $method = $this->types[$this->task->parser_params->type];

        // Список итемов
        $items = Helper::textToArray($this->task->parser_params->items);

        $items->each(function($item) use ($api, $method) {
            try {
                $this->storeMedia($api->$method($item));
            } catch (\Exception $exception) {
                // Если поймали ошибку, занесем в лог и сделаем задачу неактивной. И прервем выполнение
                $this->task->update([
                    'active' => false,
                ]);

                UserLog::add($this->task->user,
                    'Ошибка парсера Instagram, задача #' . $this->task->id . ' "' . $this->task->name . '"' . "\n" . $exception->getMessage(),
                    UserLog::LEVEL_ERROR
                );

                return false;
            }

            return true;
        });
    }


    /**
     * @param Collection $media
     */
    private function storeMedia($media)
    {
        $media->each(function($item) {

            $media_id = sprintf('%s_%s',
                $this->task->parser_type,
                $item['id']
            );

            $media = [
                '_id' => $media_id,
                'parser_type' => $this->task->parser_type,
                'group_id' => $this->task->group->id,
                'url' => $item['display_src'],
                'link' => 'https://www.instagram.com/p/' . $item['code'] . '/',
                'description' => isset($item['caption']) ? $item['caption'] : '',
            ];

            $this->store(new Media($media));
        });
    }

    /**
     * Параметры парсера по умолчанию
     *
     * @return array
     */
    public static function getDefaultParams()
    {
        return [
            'login' => '',
            'password' => '',
            'type' => 0,
            'items' => '',
        ];
    }

    /**
     * Правила валидации параметров парсера
     *
     * @return array
     */
    public static function getValidationRules()
    {
        return [
            'parser_params.items' => 'required'
        ];
    }

    /**
     * Сообщение валидатора об ошибках
     *
     * @return array
     */
    public static function getValidationMessages()
    {
        return [
            'parser_params.items.required' => 'Поле "Теги или аккаунты" должно быть заполнено'
        ];
    }
}
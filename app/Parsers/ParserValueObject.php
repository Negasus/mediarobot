<?php

namespace App\Parsers;

/**
 * Class ParserValueObject
 * @package App\Parsers
 */
class ParserValueObject
{
    public $id;

    public $class;

    public $name;

    public $description;

    /**
     * ParserValueObject constructor.
     *
     * @param $id
     * @param $class
     * @param $name
     * @param $description
     */
    function __construct($id = null, $class = null, $name = null, $description = null)
    {
        $this->id = $id;
        $this->class = $class;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass(string $class)
    {
        $this->class = $class;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

}
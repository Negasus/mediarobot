<?php

namespace App\Parsers\VK\Api;

use GuzzleHttp\Client;

class VKApi
{
    protected $version = '5.67';

    protected $url = 'https://api.vk.com/method/%s?access_token=%s&v=%s';

    private $client;

    private $token;

    /**
     * VKApi constructor.
     * @param string $token Access token
     */
    function __construct($token)
    {
        $this->client = new Client();

        $this->token = $token;
    }

    /**
     * @param $method
     * @param array $params
     * @return mixed
     */
    public function call($method, $params = [])
    {
        $url = sprintf($this->url,
            $method,
            $this->token,
            $this->version
            );

        foreach ($params as $key => $value) {
            $url .= '&' . $key . '=' . $value;
        }

        $response = $this->client->get($url);

        return json_decode($response->getBody()->getContents(), true);
    }
}
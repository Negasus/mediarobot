<?php

namespace App\Parsers\VK;

use App\Models\Media;
use App\Parsers\BaseParser;
use App\Contracts\ParserContract;
use App\Parsers\VK\Api\VKApi;
use App\Services\UserLog;

class VK extends BaseParser implements ParserContract
{
    /**
     * @return mixed|void
     */
    public function run()
    {
        $api = new VKApi($this->task->parser_params->token);

        $result = $api->call('wall.get', [
            'owner_id' => $this->task->parser_params->id,
        ]);

        //  Ошибка парсинга
        if (isset($result['error'])) {
            UserLog::add($this->task->user, 'Ошибка парсера VK. Задача #' . $this->task->id . '. Сообщение: ' . $result['error']['error_msg']);

            $this->task->update([
                'active' => false,
            ]);

            return;
        }

        collect($result['response']['items'])->each(function($item){
            $this->processItem($item);
        });

    }

    /**
     * @param $item
     */
    private function processItem($item) {

        if (! isset($item['attachments']) || $this->isPassed($item)) {
            return;
        }

        collect($item['attachments'])->each(function($attachment) use ($item) {

            $method = 'processMedia' . ucfirst(strtolower($attachment['type']));

            if (method_exists($this, $method)) {
                $this->$method('', $item['text'], $attachment);
            }

        });
    }

    /**
     * @param $title
     * @param $description
     * @param $attachment
     */
    public function processMediaPhoto($title, $description, $attachment)
    {
        $sizes = ['1280', '807', '604', '130', '75'];

        $media_id = sprintf('%s_%s_%s',
            $this->task->parser_type,
            $attachment['photo']['owner_id'],
            $attachment['photo']['id']
        );

        $media = [
            '_id' => $media_id,
            'title' => $title,
            'description' => $description,
            'parser_type' => $this->task->parser_type,
            'group_id' => $this->task->group->id,
        ];

        // Проходим по всем размерам, и берем наибольший
        foreach ($sizes as $size) {
            if (isset($attachment['photo']['photo_' . $size])) {
                $media['url'] = $attachment['photo']['photo_' . $size];
                break;
            }
        }

        $this->store(new Media($media));
    }

    /**
     * Если медиа - рекламный или прикрепленный пост - не обрабатываем
     *
     * @param $item
     * @return bool
     */
    private function isPassed($item)
    {
        return $item['marked_as_ads'] == 1 || (isset($item['is_pinned']) &&  $item['is_pinned'] == 1);
    }

    /**
     * @inheritdoc
     */
    public static function getDefaultParams()
    {
        return [
            'id' => 0,
            'token' => '',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getValidationRules()
    {
        return [
            'parser_params.id' => 'required|integer',
            'parser_params.token' => 'required|string'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getValidationMessages()
    {
        return [
            'parser_params.id.required' => 'Поле ID в настройках парсера должно быть заполнено',
            'parser_params.id.integer' => 'Поле ID в настройках парсера должно быть числом',
            'parser_params.token.required' => 'Поле "Ключ доступа" в настройках парсера должно быть заполнено',
            'parser_params.token.string' => 'Поле "Ключ доступа" в настройках парсера должно быть строкой',
        ];
    }
}
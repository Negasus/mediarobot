<?php

namespace App\Parsers\Tumblr;

use App\Contracts\ParserContract;
use App\Models\Media;
use App\Parsers\BaseParser;
use Tumblr\API\Client;

/**
 * Class Tumblr
 * @package App\Parsers\Tumblr
 */
class Tumblr extends BaseParser implements ParserContract
{

    /**
     * @var array
     */
    private $types = [
        0 => 'getDashboardPosts'
    ];

    /**
     * @inheritdoc
     */
    public function run()
    {
        $client = new Client(
            $this->task->parser_params->consumer_key,
            $this->task->parser_params->consumer_secret,
            $this->task->parser_params->token,
            $this->task->parser_params->token_secret
            );

        $method = $this->types[$this->task->parser_params->type];

        $this->$method($client)->each(function($item) {

            $itemMethod = 'processMedia' . ucfirst(strtolower($item->type));

            if (method_exists($this, $itemMethod)) {
                $this->$itemMethod($item);
            }

        });

    }

    /**
     * @param Client $client
     * @return \Illuminate\Support\Collection
     */
    private function getDashboardPosts($client)
    {
        return collect($client->getDashboardPosts()->posts);
    }

    /**
     * @param $item
     */
    public function processMediaLink($item)
    {
        collect($item->photos)->each(function($photo, $key) use ($item) {
            $id = sprintf('%s_%s_%s',
                $this->task->parser_type,
                $item->id,
                $key
            );

            $media = new Media([
                '_id' => $id,
                'parser_type' => $this->task->parser_type,
                'group_id' => $this->task->group->id,
                'url' => $photo->original_size->url,
                'link' => $item->post_url,
                'tags' => $item->tags,
                'title' => $item->title,
                'description' => '',
            ]);

            $this->store($media);
        });
    }

    /**
     * @param $item
     */
    public function processMediaVideo($item)
    {
        $id = sprintf('%s_%s',
            $this->task->parser_type,
            $item->id
        );

        $media = new Media([
            '_id' => $id,
            'type_id' => 2, // fixme: hardcoded type Video
            'parser_type' => $this->task->parser_type,
            'group_id' => $this->task->group->id,
            'url' => $item->video_url,
            'link' => $item->post_url,
            'tags' => $item->tags,
            'description' => isset($item->caption) ? $item->caption : '',
        ]);

        $this->store($media);
    }

    /**
     * @param $item
     */
    private function processMediaPhoto($item) {
        collect($item->photos)->each(function($photo, $key) use ($item) {
            $id = sprintf('%s_%s_%s',
                $this->task->parser_type,
                $item->id,
                $key
            );

            $media = new Media([
                '_id' => $id,
                'parser_type' => $this->task->parser_type,
                'group_id' => $this->task->group->id,
                'url' => $photo->original_size->url,
                'link' => $item->post_url,
                'tags' => $item->tags,
                'description' => isset($item->caption) ? $item->caption : '',
            ]);

            $this->store($media);
        });
    }

    /**
     * @inheritdoc
     */
    public static function getDefaultParams()
    {
        return [
            'consumer_key' => '',
            'consumer_secret' => '',
            'token' => '',
            'token_secret' => '',
            'type' => 0,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getValidationRules()
    {
        return [
            'parser_params.consumer_key' => 'required',
            'parser_params.consumer_secret' => 'required',
            'parser_params.token' => 'required',
            'parser_params.token_secret' => 'required',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getValidationMessages()
    {
        return [
            'parser_params.consumer_key.required' => 'Поле "Consumer Key" должно быть заполнено',
            'parser_params.consumer_secret.required' => 'Поле "Consumer Secret" должно быть заполнено',
            'parser_params.token.required' => 'Поле "Token" должно быть заполнено',
            'parser_params.token_secret.required' => 'Поле "Token Secret" должно быть заполнено',
        ];
    }

}
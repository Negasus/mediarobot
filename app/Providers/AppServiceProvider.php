<?php

namespace App\Providers;

use Cron\CronExpression;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // For fix mysql error: 1071 Specified key was too long; max key length is 767 bytes
        Schema::defaultStringLength(191);

        \Validator::extend('cron', function($attribute, $value, $parameters, $validator) {
            return CronExpression::isValidExpression($value);
        });

        \Validator::extend('regexpression', function($attribute, $value, $parameters, $validator) {
            try {
                return preg_match($value, '') !== false;
            } catch (\Exception $e) {
                return false;
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}

<?php

namespace App\Providers;

use App\Repositories\Filter\Filter\EloquentFilterRepository;
use App\Repositories\Filter\Filter\FilterRepositoryInterface;
use App\Repositories\Filter\FilterType\ConfigFilterTypeRepository;
use App\Repositories\Filter\FilterType\FilterTypeRepositoryInterface;
use App\Repositories\Group\EloquentGroupRepository;
use App\Repositories\Group\GroupRepositoryInterface;
use App\Repositories\Media\EloquentMediaRepository;
use App\Repositories\Media\MediaRepositoryInterface;
use App\Repositories\Parser\ConfigParserRepository;
use App\Repositories\Parser\ParserRepositoryInterface;
use App\Repositories\Publisher\EloquentPublisherRepository;
use App\Repositories\Publisher\PublisherRepositoryInterface;
use App\Repositories\Task\EloquentTaskRepository;
use App\Repositories\Task\TaskRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Bind repositories
        $this->app->singleton(TaskRepositoryInterface::class, EloquentTaskRepository::class);
        $this->app->singleton(ParserRepositoryInterface::class, ConfigParserRepository::class);
        $this->app->singleton(GroupRepositoryInterface::class, EloquentGroupRepository::class);
        $this->app->singleton(PublisherRepositoryInterface::class, EloquentPublisherRepository::class);
        $this->app->singleton(MediaRepositoryInterface::class, EloquentMediaRepository::class);
        $this->app->singleton(FilterRepositoryInterface::class, EloquentFilterRepository::class);
        $this->app->singleton(FilterTypeRepositoryInterface::class, ConfigFilterTypeRepository::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

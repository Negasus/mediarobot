<?php

namespace App\Providers;

use App\Models\Filter;
use App\Models\Group;
use App\Models\Publisher;
use App\Models\Task;
use App\Policies\FilterPolicy;
use App\Policies\GroupPolicy;
use App\Policies\PublisherPolicy;
use App\Policies\TaskPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Task::class => TaskPolicy::class,
        Group::class => GroupPolicy::class,
        Filter::class => FilterPolicy::class,
        Publisher::class => PublisherPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}

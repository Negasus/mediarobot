<?php

namespace App\Publishers;

use App\Models\Media;
use App\Models\Publisher;
use App\Repositories\Media\MediaRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class BasePublisher
 * @package App\Publishers
 */
class BasePublisher
{
    /**
     * @var Publisher
     */
    protected  $publisher;

    /**
     * @var Collection
     */
    protected $media;

    /**
     * BasePublisher constructor.
     * @param Publisher $publisher
     */
    function __construct(Publisher $publisher)
    {
        $this->publisher = $publisher;

        $this->getMedia();
    }

    /**
     * Get media and set 'processed' status
     *
     */
    protected function getMedia()
    {
        /**
         * @var MediaRepositoryInterface $mediaRepository
         */
        $mediaRepository = app(MediaRepositoryInterface::class);

        \DB::transaction(function() use ($mediaRepository) {

            $this->media = $this->publisher
                ->group
                ->media()
                ->where('status', Media::STATUS_APPROVED)
                ->orderBy('created_at', 'asc')
                ->take($this->publisher->publisher_params->count)
                ->lockForUpdate()
                ->get();

            $mediaRepository->massUpdate($this->media->pluck('id')->toArray(), [
                'status' => Media::STATUS_PROCESSED,
            ]);

        });
    }
}
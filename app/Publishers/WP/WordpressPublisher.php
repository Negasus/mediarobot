<?php

namespace App\Publishers\WP;

use App\Contracts\PublisherContract;
use App\Models\Media;
use App\Publishers\BasePublisher;
use App\Publishers\WP\Api\WpCli;
use App\Services\UserLog;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class WordpressPublisher extends BasePublisher implements PublisherContract
{
    /**
     * @var WpCli
     */
    private $api;

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->api = new WpCli($this);

        $path = $this->publisher->publisher_params->path;

        try {
            $this->media->each(function(Media $media) use ($path) {

                if ($this->api->post($path, $media)) {
                    $media->update([
                        'published_at' => Carbon::now(),
                        'published_to' => $this->publisher->id,
                        'status' => Media::STATUS_PUBLISHED,
                    ]);
                }

            });
        } catch (\Exception $e) {
            // Если в процессе публикации медиа ловим ошибку, то отключаем публикатор и сохраняем сообщения
            $this->publisher->update([
                'active' => false,
            ]);

            UserLog::add($this->publisher->user, 'Ошибка публикатора ' . $this->publisher->name);
            \Log::error($e->getMessage());

            // А так же все медиа, которые не успели обработать, вернем статус Approved
            // todo: refactor to use repo?
            Media::whereIn('id', $this->media->pluck('id')->toArray())
                ->where('status', Media::STATUS_PROCESSED)
                ->update([
                    'status' => Media::STATUS_APPROVED,
                ]);

            return false;
        }

        return true;
    }

    private function publishMedia(Media $media)
    {

    }

    /**
     * @inheritdoc
     */
    public static function getDefaultParams()
    {
        return [
            'path' => '',
            'connection' => '',
            'count' => 1,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getValidationRules()
    {
        return [
            'publisher_params.count' => 'required|integer|min:1',
            'publisher_params.path' => 'required|string',
            'connection' => 'string|max:255',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getValidationMessages()
    {
        return [
            'publisher_params.count.required' => 'Поле "Количество медиа" должно быть заполнено',
            'publisher_params.count.integer' => 'Значение поля "Количество медиа" должно быть числом',
            'publisher_params.count.min' => 'Значение поля "Количество медиа" должно быть больше нуля',
            'publisher_params.path.required' => 'Поле "Путь к Wordpress" должно быть заполнено',
        ];
    }
}
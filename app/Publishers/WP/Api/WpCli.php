<?php

namespace App\Publishers\WP\Api;

use App\Models\Media;
use App\Services\UserLog;
use Symfony\Component\Process\Process;

/**
 * Class WpCli
 * @package App\Publishers\WP\Api
 */
class WpCli
{
    /**
     * @var string
     */
    private $bin;
    /**
     * @var
     */
    private $publisher;

    /**
     * WpCli constructor.
     * @param $publisher
     */
    function __construct($publisher)
    {
        $this->bin = base_path('app/bin/wp-cli.phar');
        $this->publisher = $publisher;
    }

    /**
     * @param $path
     * @param Media $media
     * @return bool
     * @throws \Exception
     */
    public function post($path, Media $media)
    {
        $description = $this->createDescription($media);

        $process = $this->createProcess($path, 'post create', [
            'post_status' => 'publish',
            'post_title' => ($media->title) ?: '',
            'post_content' => $description,
            'porcelain' => null,
        ]);

        $process->run();

        if (! $process->isSuccessful()) {
            $media->update([
                'status' => Media::STATUS_BLOCKED,
            ]);

            UserLog::add($media->task->user, 'Ошибка публикатора ' . $this->publisher->name);
            \Log::info('WpCli: Error create post with media #' . $media->id . ' to path: ' . $path . ". Media blocked.\n" . $process->getErrorOutput());
            return false;
        }

        $id = trim($process->getOutput());

        // Прикрепляем теги
        foreach ($media->tags as $tag) {
            $process = $this->createProcess($path, 'post term add ' . $id . ' post_tag ' . $tag);

            $process->run();
        }

        return true;
    }

    /**
     * @param Media $media
     * @return mixed
     */
    public function createDescription(Media $media)
    {
//
        $method = 'description' . ucfirst(strtolower($media->type->name));
        return $this->$method($media);
    }

    /**
     * @param Media $media
     * @return string
     */
    public function descriptionPhoto(Media $media)
    {
        return $media->description . ' <img src="' . $media->url . '">';
    }

    /**
     * @param Media $media
     * @return string
     */
    public function descriptionVideo(Media $media)
    {
        return $media->description . '<video controls="controls" src="' . $media->url . '"></video>';
    }

    /**
     * @param $path
     * @param $command
     * @param array $params
     * @return Process
     */
    private function createProcess($path, $command, $params = [])
    {
        $c = $this->bin . ' ' . $command . ' --allow-root --path="' . $path . '"';
        foreach ($params as $key => $value) {
            $c .= ' --' . $key;
            if (!is_null($value)) {
                $c .= (is_int($value)) ? '=' . $value : '="' . addslashes($value) . '"';
            }
        }

        return new Process($c);
    }}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Media
 *
 * @package App\Models
 * @property int $id
 * @property string $_id
 * @property int $group_id
 * @property int $task_id
 * @property string $parser_type
 * @property string|null $title
 * @property string|null $description
 * @property string|null $url
 * @property string|null $link
 * @property string|null $tags
 * @property int $status
 * @property \Carbon\Carbon|null $published_at
 * @property string|null $published_to
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Group $group
 * @property-read \App\Models\Task $task
 * @property-read \App\Models\MediaType $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereParserType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media wherePublishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media wherePublishedTo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Media whereUrl($value)
 * @mixin \Eloquent
 */
class Media extends Model
{
    /**
     *
     */
    const STATUS_NONE       = 0;
    /**
     *
     */
    const STATUS_APPROVED   = 1;
    /**
     *
     */
    const STATUS_BLOCKED    = 2;
    /**
     *
     */
    const STATUS_PUBLISHED  = 3;

    /**
     *
     */
    const STATUS_PROCESSED  = 4;

    /**
     * @var array
     */
    protected $fillable = [
        '_id',
        'group_id',
        'type_id',
        'parser_type',
        'url',
        'link',
        'title',
        'description',
        'tags',
        'published_at',
        'published_to',
        'status',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'published_at',
    ];

    /**
     * @param $value
     * @return mixed
     */
    public function getTagsAttribute($value)
    {
        return json_decode($value, true);
    }

    /**
     * @param $value
     */
    public function setTagsAttribute($value)
    {
        $this->attributes['tags'] = json_encode($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function task()
    {
        return $this->belongsTo(Task::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(MediaType::class);
    }
}

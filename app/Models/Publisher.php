<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Publisher
 *
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property int $group_id
 * @property string $publisher_type
 * @property mixed $publisher_params
 * @property string $cron
 * @property int $active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Group $group
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher whereCron($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher wherePublisherParams($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher wherePublisherType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Publisher whereUserId($value)
 * @mixin \Eloquent
 */
class Publisher extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'group_id',
        'name',
        'publisher_type',
        'publisher_params',
        'cron',
        'active',
    ];

    /**
     * @param $value
     * @return mixed
     */
    public function getPublisherParamsAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * @param $value
     */
    public function setPublisherParamsAttribute($value)
    {
        $this->attributes['publisher_params'] = json_encode($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}

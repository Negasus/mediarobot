<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Task
 *
 * @package App\Models
 * @property int $id
 * @property int $user_id
 * @property int $group_id
 * @property string $name
 * @property string $parser_type
 * @property mixed $parser_params
 * @property string $cron
 * @property int $active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Filter[] $filters
 * @property-read \App\Models\Group $group
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereCron($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereParserParams($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereParserType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Task whereUserId($value)
 * @mixin \Eloquent
 */
class Task extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'parser_type',
        'parser_params',
        'cron',
        'active',
        'group_id',
    ];

    /**
     * @param $value
     * @return mixed
     */
    public function getParserParamsAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * @param $value
     */
    public function setParserParamsAttribute($value)
    {
        $this->attributes['parser_params'] = json_encode($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function filters()
    {
        return $this->hasMany(Filter::class);
    }
}

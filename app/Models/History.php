<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class History
 *
 * @package App\Models
 * @property int $id
 * @property int $user_id
 * @property string $level
 * @property string $message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\History whereUserId($value)
 * @mixin \Eloquent
 */
class History extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'level',
        'message'
    ];
}

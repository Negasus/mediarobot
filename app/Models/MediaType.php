<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MediaType
 * @package App\Models
 * @property string $name
 */
class MediaType extends Model
{
    /**
     * @var string
     */
    protected $table = 'media_types';

    /**
     * @var array
     */
    protected $fillable = ['name'];
}

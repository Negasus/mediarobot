<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

/**
 * Class Filter
 *
 * @package App\Models
 * @property int $id
 * @property int|null $task_id
 * @property int $user_id
 * @property int $order
 * @property string $name
 * @property string $filter_type
 * @property mixed $filter_params
 * @property int $active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Task|null $task
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Filter whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Filter whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Filter whereFilterParams($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Filter whereFilterType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Filter whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Filter whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Filter whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Filter whereTaskId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Filter whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Filter whereUserId($value)
 * @mixin \Eloquent
 */
class Filter extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'order',
        'task_id',
        'user_id',
        'name',
        'filter_type',
        'filter_params',
        'active',
    ];

    /**
     * @param $value
     */
    public function setTaskIdAttribute($value) {
        ($value == 0) ?
            $this->attributes['task_id'] = null :
            $this->attributes['task_id'] = $value;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getFilterParamsAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * @param $value
     */
    public function setFilterParamsAttribute($value)
    {
        $this->attributes['filter_params'] = json_encode($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Builder
     */
    public function task()
    {
        return $this->belongsTo(Task::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Builder
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

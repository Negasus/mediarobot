<?php

namespace App\Services;

use App\Models\User;

class UserLog
{
    const LEVEL_INFO = 'info';
    const LEVEL_ERROR = 'error';

    /**
     * @param User $user
     * @param string $message
     * @param string $level
     */
    public static function add(User $user, string $message, $level = self::LEVEL_INFO) {
        $user->history()->create([
            'message' => $message,
            'level' => $level,
        ]);
    }
}
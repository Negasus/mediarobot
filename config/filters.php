<?php

return [
    [
        'id' => 'stop_words',
        'class' => \App\Filters\StopWordsFilter::class,
        'name' => 'Стоп-слова',
        'description' => 'Отмена сохранения Медиа, если в описании или заголовке встречаются указанные стоп-слова',
    ],
    [
        'id' => 'regex',
        'class' => \App\Filters\RegExReplaceFilter::class,
        'name' => 'Замена по регулярному выражению',
        'description' => 'Позволяет заменять любую часть описания по регулярному выражению',
    ],
    [
        'id' => 'remove_hashtag',
        'class' => \App\Filters\RemoveHashtagFilter::class,
        'name' => 'Удаление хештегов из описания',
        'description' => 'Удаляет все хештеги из описания Медиа',
    ],
    [
        'id' => 'set_random_title',
        'class' => \App\Filters\SetRandomTitleFilter::class,
        'name' => 'Случайный заголовок',
        'description' => 'Установка случайного заголовка из заданного списка',
    ],
    [
        'id' => 'block_non_unique_title',
        'class' => \App\Filters\BlockNonUniqueTitleFilter::class,
        'name' => 'Блокировка не уникальных заголовков',
        'description' => 'Если у медиа заголовок, который уже есть в системе, блокировать его. Можно настроть кол-во дней, когда искать заголовок',
    ],
    [
        'id' => 'make_title',
        'class' => \App\Filters\MakeTitleFilter::class,
        'name' => 'Заголовок из описания',
        'description' => 'Создания заголовка из описания',
    ],
    [
        'id' => 'set_status',
        'class' => \App\Filters\SetStatusFilter::class,
        'name' => 'Установка статуса',
        'description' => 'Установка указанного статуса при сохранении Медиа',
    ],
    [
        'id' => 'remove_html_tags',
        'class' => \App\Filters\RemoveHTMLTagsFilter::class,
        'name' => 'Удаление HTML тегов из описания',
        'description' => 'Удаление всех HTML тегов из описания Медиа',
    ],
    [
        'id' => 'custom_tumblr_1',
        'class' => \App\Filters\Custom\Tumblr1::class,
        'name' => 'Спец фильтр Tumblr',
        'description' => 'Удаляет из описания ссылки, HTML теги, копирует описание в Загловок. Одобряет медиа, если заголовок достаточной длинны. Если описание полностью поместилось в заголовок - стираем его',
    ],
    [
        'id' => 'custom_instagram_1',
        'class' => \App\Filters\Custom\Instagram1::class,
        'name' => 'Спец фильтр Instagram',
        'description' => 'Вырегает теги, удаляет из описания ссылки, HTML теги, копирует описание в Загловок. Одобряет медиа, если заголовок достаточной длинны. Если описание полностью поместилось в заголовок - стираем его',
    ],
];
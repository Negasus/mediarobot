<?php

return [
    [
        'id' => 'wordpress',
        'class' => \App\Publishers\WP\WordpressPublisher::class,
        'name' => 'Wordpress',
        'description' => 'Публикация в блог Wordpress',
    ],
];
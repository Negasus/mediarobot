<?php

return [
    [
        'id' => 'vk',
        'class' => \App\Parsers\VK\VK::class,
        'name' => 'ВКонтакте',
        'description' => 'Парсинг медиа со стены пользователя или сообщества',
    ],
    [
        'id' => 'instagram',
        'class' => \App\Parsers\Instagram\Instagram::class,
        'name' => 'Инстаграм',
        'description' => 'Парсинг медиа из Инстаграма. По тегам, по аккаунтам',
    ],
    [
        'id' => 'tumblr',
        'class' => \App\Parsers\Tumblr\Tumblr::class,
        'name' => 'Tumblr',
        'description' => 'Парсинг медиа из Tumbler',
    ],
];
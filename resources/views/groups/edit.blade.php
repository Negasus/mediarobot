@extends('layouts.app')

@section('content')
    <div class="container">
        @if (Route::currentRouteName() == 'groups.create')
            <form method="POST" action="{{ route('groups.store') }}">
        @else
            <form method="POST" action="{{ route('groups.update', $group) }}">
            {{ method_field('PATCH') }}
        @endif
            {{ csrf_field() }}
            <input type="hidden" name="user_id" value="{{ $group->user_id }}">

            <div class="panel panel-default">

                <div class="panel-heading">
                    Группа
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label for="group-name">Имя группы</label>
                        <input class="form-control"
                               type="text"
                               name="name"
                               id="group-name"
                               value="{{ old('name', $group->name) }}">
                    </div>
                </div>

            </div>

            <div class="form-group">
                <button class="btn btn-primary">Сохранить</button>
                <a class="btn btn-link" href="{{ route('groups.index') }}">Отмена</a>
            </div>

        </form>
    </div>
@endsection
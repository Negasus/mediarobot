@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ route('groups.create') }}" class="btn btn-sm btn-default">
                    <span class="glyphicon-plus glyphicon"></span> Новая группа
                </a>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Имя</th>
                            <th>Количество медиа</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($groups as $group)
                            <tr>
                                <td>
                                    {{ $group->id }}
                                </td>
                                <td>
                                    <a href="{{ route('groups.edit', $group) }}">{{ $group->name }}</a>
                                </td>
                                <td>
                                    {{ $group->media()->count() }}
                                </td>
                                <td>
                                    <form action="{{ route('groups.destroy', $group) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-sm btn-danger">
                                            <span class="glyphicon-trash glyphicon"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
@extends('layouts.app')

@section('content')
    <div class="container">

        @if (Route::currentRouteName() == 'publishers.create')
            <form method="POST" action="{{ route('publishers.store') }}">
        @else
            <form method="POST" action="{{ route('publishers.update', $publisher) }}">
            {{ method_field('PATCH') }}
        @endif
            {{ csrf_field() }}

            <div class="panel panel-default">
                <div class="panel-heading">
                    Публикатор
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <label>Тип публикатора</label>
                        <input class="form-control"
                               name="publisher_type"
                               type="text"
                               readonly
                               value="{{ $publisher->publisher_type }}">
                    </div>

                    <div class="form-group">
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="active" type="checkbox" @if ($publisher->active) checked @endif> Активно
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="publisher-name">Имя публикатора</label>
                        <input class="form-control"
                               type="text"
                               name="name"
                               id="publisher-name"
                               value="{{ old('name', $publisher->name) }}">
                    </div>

                    <div class="form-group">
                        <label for="publisher-group-id" class="control-label">Группа</label>
                        <select name="group_id" id="publisher-group-id" class="form-control">
                            @foreach($groups as $group)
                                <option @if($publisher->group_id == $group->id) selected @endif value="{{ $group->id }}">{{ $group->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    @include('common.cron-field', ['cron' => $publisher->cron])

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Настройки публикатора
                </div>
                <div class="panel-body">
                    @include('publishers.publishers-spec.' . $publisherShortName)
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-primary">Сохранить</button>
                <a class="btn btn-link" href="{{ route('publishers.index') }}">Отмена</a>
            </div>

        </form>
    </div>
@endsection
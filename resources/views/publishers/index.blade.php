@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <button data-toggle="modal" data-target="#new-publisher-modal" class="btn btn-sm btn-default">
                    <span class="glyphicon-plus glyphicon"></span> Добавить публикатор
                </button>
            </div>
            <div class="panel-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Имя</th>
                            <th>Тип</th>
                            <th>Группа</th>
                            <th>Расписание</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($publishers as $publisher)
                            <tr>
                                <td>
                                    <span class="label @if ($publisher->active) label-success @else label-default @endif">{{ $publisher->id }}</span>
                                </td>
                                <td>
                                    <a href="{{ route('publishers.edit', $publisher) }}">{{ $publisher->name }}</a>
                                </td>
                                <td>
                                    {{ $publisher->publisher_type }}
                                </td>
                                <td>
                                    <a href="{{ route('groups.edit', $publisher->group) }}">{{ $publisher->group->name }}</a>
                                </td>
                                <td>
                                    {{ $publisher->cron }}
                                </td>
                                <td>
                                    <form method="POST" action="{{ route('publishers.run', $publisher) }}">
                                        {{ csrf_field() }}
                                        <button class="btn btn-sm btn-info"  data-toggle="tooltip" data-placement="top" title="Выполнить">
                                            <span class="glyphicon-play glyphicon"></span>
                                        </button>
                                    </form>
                                </td>
                                <td>
                                    <form method="POST" action="{{ route('publishers.destroy', $publisher) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-sm btn-danger"  data-toggle="tooltip" data-placement="top" title="Удалить">
                                            <span class="glyphicon-trash glyphicon"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('publishers.partials.new-publisher-modal')
@endsection
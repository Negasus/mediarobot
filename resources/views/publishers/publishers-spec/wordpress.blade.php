<div class="form-group">
    <label for="publisher-wordpress-count" class="control-label">Количество медиа, которые берет публикатор из группы за один запуск</label>
    <input type="number"
           class="form-control"
           id="publisher-wordpress-count"
           name="publisher_params[count]"
           value="{{ old('publisher_params.count', $publisher->publisher_params->count) }}">
</div>

<div class="form-group">
    <label class="control-label" for="publisher-wordpress-path">Путь к блогу Wordpress</label>
    <input class="form-control"
           name="publisher_params[path]"
           id="publisher-wordpress-path"
           value="{{ old('publisher_params.path', $publisher->publisher_params->path) }}">
    <span class="help-block">Полный путь к блогу Wordpress</span>
</div>

<div class="form-group">
    <label class="control-label" for="publisher-wordpress-connection">Параметры подключения к удаленному серверу</label>
    <input class="form-control"
           name="publisher_params[connection]"
           id="publisher-wordpress-connection"
           value="{{ old('publisher_params.connection', $publisher->publisher_params->connection) }}">
    <span class="help-block">Оставьте строку пустой, если блог находится на этом же сервере<br>
        Формат: <code>login@domain.com</code>
    </span>
</div>

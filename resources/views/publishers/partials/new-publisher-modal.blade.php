<div id="new-publisher-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Создание нового публикатора</h4>
            </div>
            <div class="modal-body">
                <p>
                    Выберите тип публикатора
                </p>
                <div class="list-group">
                    @foreach($publisherTypes as $publisher)
                        <a href="{{ route('publishers.create', ['publisherShortName' => $publisher['id']]) }}"
                           class="list-group-item">
                            <h4 class="list-group-item-heading">{{ $publisher['name'] }}</h4>
                            <p class="list-group-item-text">{{ $publisher['description'] }}</p>
                        </a>
                    @endforeach
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>

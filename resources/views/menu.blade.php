<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                <li @if(Route::currentRouteName() == 'dashboard.index') class="active" @endif><a href="{{ route('dashboard.index') }}">Главная</a></li>
                <li @if(Route::currentRouteName() == 'tasks.index') class="active" @endif><a href="{{ route('tasks.index') }}">Задачи</a></li>
                <li @if(Route::currentRouteName() == 'media.index') class="active" @endif><a href="{{ route('media.index') }}">Медиа</a></li>
                <li @if(Route::currentRouteName() == 'groups.index') class="active" @endif><a href="{{ route('groups.index') }}">Группы</a></li>
                <li @if(Route::currentRouteName() == 'filters.index') class="active" @endif><a href="{{ route('filters.index') }}">Фильтры</a></li>
                <li @if(Route::currentRouteName() == 'publishers.index') class="active" @endif><a href="{{ route('publishers.index') }}">Публикаторы</a></li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if (Auth::guest())
                    <li><a href="{{ route('login') }}">Login</a></li>
                    <li><a href="{{ route('register') }}">Register</a></li>
                @else
                    <li>
                        <a href="{{ route('log.index') }}">Лог
                            @if (Auth::user()->history()->count() > 0)
                            <span class="badge badge-danger">{{ Auth::user()->history()->count() }}</span>
                            @endif
                        </a>
                    </li>
                    <li><a href="{{ route('log.system.index') }}">Системный лог</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('dashboard.phpinfo') }}">PHPInfo</a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                    Выход
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

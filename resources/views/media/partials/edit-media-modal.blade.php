<div id="edit-media-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Редактирование медиа</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="edit-media-id">
                <div class="form-group">
                    <label for="edit-media-title" class="control-label">Заголовок</label>
                    <input id="edit-media-title" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="edit-media-description" class="control-label">Описание</label>
                    <textarea id="edit-media-description" class="form-control" rows="10"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="edit-media-save">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </div>
    </div>
</div>

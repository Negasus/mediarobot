@extends('layouts.app')

@section('head-scripts')
    <link rel="stylesheet" href="/css/fancybox.css">
@endsection

@section('body-scripts')
    <script src="/js/fancybox.js"></script>
    <script src="/js/pages/media-index.js"></script>
@endsection

@section('content')
    <div class="container">
        <div class="jumbotron">
            <media-filter filters="{{ \Illuminate\Support\Facades\Request::get('filters') }}"
                          :groups="{{ $groups }}"
                          :user-filters="{{ $filters }}"
                          current-group="{{ Request::get('group') }}"
                          count="{{ $count }}"></media-filter>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading" style="text-align: right">Цвет строки:
                <span class="label label-default">Новые</span>
                <span class="label label-info">Одобренные</span>
                <span class="label label-warning">В процессе публикации</span>
                <span class="label label-danger">Заблокированные</span>
                <span class="label label-success">Опубликованные</span>
            </div>
            <div class="panel-body">
                <form id="form" action="{{ route('media.update') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="search" value="">
                    <div class="row">
                        <div class="col-lg-5">
                            <select name="action" id="action" class="form-control">
                                <optgroup label="Действия">
                                    <option value="none">Ничего не делать</option>
                                    <option value="approve">Установить статус "Одобрено"</option>
                                    <option value="block">Установить статус "Заблокировано"</option>
                                    {{--<option value="publish">Опубликовать в ...</option>--}}
                                    <option value="unmark">Снять все отметки (статус "Новое")</option>
                                    <option value="delete">Удалить</option>
                                </optgroup>
                                <optgroup label="Применить фильтр">
                                    @foreach($filters as $filter)
                                        <option value="filter:{{ $filter->id }}">{{ $filter->name }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <button id="apply-action" class="btn btn-default">Применить к отмеченным</button>
                        </div>
                    </div>


                    <table class="table table-hover table-media">
                        <thead>
                            <tr>
                                <th width="30px">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="cb-check-all">
                                        </label>
                                    </div>
                                </th>
                                <th>#</th>
                                <th>Изображение</th>
                                <th>Группа</th>
                                <th>Заголовок</th>
                                <th>Описание</th>
                                <th>Задача</th>
                                <th>Ссылка</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($media as $item)
                                <tr data-id="{{ $item->id }}" class="@if ($item->status == \App\Models\Media::STATUS_APPROVED) info @elseif($item->status == \App\Models\Media::STATUS_BLOCKED) danger @elseif($item->status == \App\Models\Media::STATUS_PROCESSED) warning @elseif($item->status == \App\Models\Media::STATUS_PUBLISHED) success @endif">
                                    <td style="vertical-align: middle">
                                        <div class="checkbox">
                                            <label>
                                                <input name="check[{{ $item->id }}]" class="cb-item" type="checkbox">
                                            </label>
                                        </div>
                                    </td>
                                    <td>
                                        {{ $item->id }}
                                    </td>
                                    <td>
                                        <a href="{{ $item->url }}" data-fancybox data-caption="">
                                            <img width="100" height="100" src="{{ $item->url }}" alt="" />
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ route('groups.edit', $item->group) }}">{{ $item->group->name }}</a>
                                    </td>
                                    <td class="title">{{ $item->title }}</td>
                                    <td>
                                        <div class="description" style="max-width: 500px; word-wrap: break-word">{{ $item->description }}</div>
                                    </td>
                                    <td>
                                        @if ($item->task)
                                            <a href="{{ route('tasks.edit', $item->task) }}">{{ $item->task->name }}</a>
                                        @else
                                            Задача не найдена
                                        @endif
                                    </td>
                                    <td>
                                        @if ($item->link)
                                            <a target="_blank" href="{{ $item->link }}">Открыть</a>
                                        @else
                                            Нет
                                        @endif
                                    </td>
                                    <td>
                                        <button data-id="{{ $item->id }}" class="btn btn-default edit-media-button">
                                            <span class="glyphicon-pencil glyphicon"></span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $media->links() }}
                </form>
            </div>
        </div>
    </div>

    @include('media.partials.edit-media-modal')
@endsection
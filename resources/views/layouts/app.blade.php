<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @yield('head-scripts')
</head>
<body>
    <div id="app">
        @include('menu')

        @if ($errors->count())
            <div class="container">
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        @yield('content')

    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    @yield('body-scripts')

    @if (session('flash'))
        <script>
            (function(){
                @foreach(session('flash') as $type => $message)
                    new Noty({
                    type: '{{ $type }}',
                    text: '{{ $message }}',
                    theme: 'metroui',
                    timeout: 3000
                }).show();
                @endforeach
            })();
        </script>
    @endif

</body>
</html>

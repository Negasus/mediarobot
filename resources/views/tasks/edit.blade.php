@extends('layouts.app')

@section('content')
    <div class="container">
        @if (Route::currentRouteName() == 'tasks.create')
            <form action="{{ route('tasks.store') }}" method="POST">
        @else
            <form action="{{ route('tasks.update', $task) }}" method="POST">
            {{ method_field('PATCH') }}
        @endif
            {{ csrf_field() }}

            <div class="panel panel-default">
                <div class="panel-heading">
                    Основные
                </div>
                <div class="panel-body">
                    <div class="form-group">

                        <label for="parser_type" class="control-label">Тип парсера</label>
                        <input class="form-control" type="text" readonly id="parser_type" name="parser_type" value="{{ $task->parser_type }}">

                    </div>

                    <div class="form-group">
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input name="active" type="checkbox" @if ($task->active) checked @endif> Активна
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name" class="control-label">Имя задачи</label>
                        <div>
                            <input type="text"
                                   name="name"
                                   class="form-control"
                                   id="name"
                                   value="{{ old('name', $task->name) }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="group" class="control-label">Группа, в которую будут помещаться Медиа, полученные этой задачей</label>
                        <div class="">
                            <select name="group_id" class="form-control" id="group">
                                @foreach($groups as $group)
                                    <option @if($task->group_id == $group->id) selected @endif value="{{ $group->id }}">{{ $group->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    @include('common.cron-field', ['cron' => $task->cron])

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    Настройки парсера
                </div>
                <div class="panel-body">
                    @include('tasks.parsers.' . $parser->id)
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-primary">Сохранить</button>
                <a class="btn btn-link" href="{{ route('tasks.index') }}">Отмена</a>
            </div>
        </form>
    </div>
@endsection
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <button data-toggle="modal" data-target="#new-task-modal" class="btn btn-sm btn-default">
                    <span class="glyphicon-plus glyphicon"></span> Новая задача
                </button>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Имя</th>
                            <th>Парсер</th>
                            <th>Группа</th>
                            <th>Расписание</th>
                            <th>Фильтры</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                            <tr>
                                <td>
                                    <span class="label @if ($task->active) label-success @else label-default @endif">{{ $task->id }}</span>
                                </td>
                                <td><a href="{{ route('tasks.edit', $task) }}">{{ $task->name }}</a></td>
                                <td>
                                    {{ $task->parser_type }}
                                </td>
                                <td>
                                    <a href="{{ route('groups.edit', $task->group) }}">{{ $task->group->name }}</a>
                                </td>
                                <td>{{ $task->cron }}</td>
                                <td>
                                    <a href="{{ route('filters.index', ['task' => $task->id]) }}" class="">Изменить</a><br>
                                    @foreach($task->filters()->orderBy('order', 'asc')->get() as $filter)
                                        {{ $filter->name }}<br>
                                    @endforeach
                                </td>
                                <td>
                                    <form action="{{ route('tasks.run', $task) }}" method="POST">
                                        {{ csrf_field() }}
                                        <button class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Выполнить">
                                            <span class="glyphicon-play glyphicon"></span>
                                        </button>
                                    </form>
                                </td>
                                <td>
                                    <form action="{{ route('tasks.destroy', $task) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Удалить">
                                            <span class="glyphicon-trash glyphicon"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('tasks.partials.new-task-modal')

@endsection
<div class="form-group">
    <label for="instagram-login" class="control-label">Имя пользователя</label>
    <div>
        <input name="parser_params[login]"
               type="text"
               class="form-control"
               value="{{ old('parser_params.login', $task->parser_params->login) }}"
               id="instagram-login">
    </div>
</div>

<div class="form-group">
    <label for="instagram-password" class="control-label">Пароль</label>
    <div>
        <input name="parser_params[password]"
               type="text"
               class="form-control"
               value="{{ old('parser_params.password', $task->parser_params->password) }}"
               id="instagram-password">
    </div>
</div>

<div class="form-group">
    <label for="instagram-type" class="control-label">Тип парсера</label>
    <div>
        <select name="parser_params[type]" class="form-control" id="instagram-type">
            <option @if($task->parser_params->type == 0) selected @endif value="0">Популярное по тегу</option>
            <option @if($task->parser_params->type == 1) selected @endif value="1">Последнее по тегу</option>
            <option @if($task->parser_params->type == 2) selected @endif value="2">Последние медиа из аккаунта</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="instagram-items" class="control-label">Теги или аккаунты</label>
    <div>
        <textarea name="parser_params[items]"
                  rows="10"
                  class="form-control"
                  id="instagram-items">{{ old('parser_params.items', $task->parser_params->items) }}</textarea>
        <span class="help-block">Каждый тег/аккаунт на отдельной строке. Задача будет обрабатывать все элементы по порядку</span>
    </div>
</div>

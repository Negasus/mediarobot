<div class="form-group">
    <label for="vk-id">ID</label>
    <input type="text"
           name="parser_params[id]"
           id="vk-id"
           class="form-control"
           value="{{ old('parser_params.id', $task->parser_params->id) }}">
    <span class="help-block">Идентификатор пользователя или сообщества (со знаком минус)</span>
</div>

<div class="form-group">
    <label for="vk-token">Ключ доступа</label>
    <input type="text"
           name="parser_params[token]"
           id="vk-token"
           class="form-control"
           value="{{ old('parser_params.token', $task->parser_params->token) }}">
</div>
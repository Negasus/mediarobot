<div class="form-group">
    <label class="control-label" for="tumblr-consumer-key">Consumer Key</label>
    <input type="text"
           name="parser_params[consumer_key]"
           id="tumblr-consumer-key"
           class="form-control"
           value="{{ old('parser_params.consumer_key', $task->parser_params->consumer_key) }}">
</div>

<div class="form-group">
    <label class="control-label" for="tumblr-consumer-secret">Consumer Secret</label>
    <input type="text"
           name="parser_params[consumer_secret]"
           id="tumblr-consumer-secret"
           class="form-control"
           value="{{ old('parser_params.consumer_secret', $task->parser_params->consumer_secret) }}">
</div>

<div class="form-group">
    <label class="control-label" for="tumblr-token">Token</label>
    <input type="text"
           name="parser_params[token]"
           id="tumblr-token"
           class="form-control"
           value="{{ old('parser_params.token', $task->parser_params->token) }}">
</div>

<div class="form-group">
    <label class="control-label" for="tumblr-token-secret">Token Secret</label>
    <input type="text"
           name="parser_params[token_secret]"
           id="tumblr-token-secret"
           class="form-control"
           value="{{ old('parser_params.token_secret', $task->parser_params->token_secret) }}">
</div>

<div class="form-group">
    <label class="control-label" for="tumblr-type">Тип парсера</label>
    <select class="form-control" name="parser_params[type]" id="tumblr-type">
        <option @if ($task->parser_params->type == 0) selected @endif value="0">Dashboard</option>
    </select>
</div>


@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <form action="{{ route('log.clear') }}" method="POST">
                    {{ csrf_field() }}
                    <button class="btn btn-sm btn-default">
                        <span class="glyphicon-trash glyphicon"></span> Очистить
                    </button>
                </form>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Created at</th>
                        <th>Level</th>
                        <th>Message</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($history as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->created_at }}</td>
                            <td>{{ $item->level }}</td>
                            <td>{{ $item->message }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
<label class="control-label" for="select-task">Задача:</label>
<select class="form-control" name="task_id" id="select-task">
    <option value="0">Общие фильтры</option>
    @foreach(Auth::user()->tasks as $item)
        <option @if((isset($task) && $task->id == $item->id) || (isset($filter) && $filter->task_id == $item->id)) selected @endif value="{{ $item->id }}">Задача "{{ $item->name }}"</option>
    @endforeach
</select>

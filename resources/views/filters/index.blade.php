@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                @include('filters.partials.tasks-select')
            </div>
            <div class="panel-body">
                <button data-toggle="modal" data-target="#new-filter-modal" class="btn btn-sm btn-default">
                    <span class="glyphicon-plus glyphicon"></span> Добавить фильтр
                </button>
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Имя</th>
                            <th>Порядок выполнения</th>
                            <th>Тип</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($filters as $filter)
                            <tr>
                                <td>
                                    {{ $filter->id }}
                                </td>
                                <td>
                                    <a href="{{ route('filters.edit', $filter) }}">{{ $filter->name }}</a>
                                </td>
                                 <td>
                                     {{ $filter->order }}
                                 </td>
                                <td>
                                    {{ $filter->filter_type }}
                                </td>
                                <td>
                                    <form action="{{ route('filters.destroy', $filter) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-sm btn-danger">
                                            <span class="glyphicon-trash glyphicon"></span>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('filters.partials.new-filter-modal')

@endsection

@section('body-scripts')
    <script>
        $(document).ready(function(){
            $('#select-task').change(function() {
                document.location.search = '?task=' + $(this).val();
            });
        });
    </script>
@endsection
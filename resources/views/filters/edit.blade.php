@extends('layouts.app')

@section('content')
    <div class="container">
        @if (Route::currentRouteName() == 'filters.create')
            <form method="post" action="{{ route('filters.store', $filter) }}">
        @else
            <form method="post" action="{{ route('filters.update', $filter) }}">
            {{ method_field('PATCH') }}
        @endif
            {{ csrf_field() }}

            <div class="panel panel-default">
                <div class="panel-heading">
                    @include('filters.partials.tasks-select')
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <label for="filter-type">Тип фильтра</label>
                        <input class="form-control"
                               type="text"
                               name="filter_type"
                               id="filter-type"
                               readonly
                               value="{{ $filter->filter_type }}">
                    </div>

                    <div class="form-group">
                        <label for="filter-name">Имя фильтра</label>
                        <input class="form-control"
                               type="text"
                               name="name"
                               id="filter-name"
                               value="{{ old('name', $filter->name) }}">
                    </div>

                    <div class="form-group">
                        <label for="filter-order">Порядок выполнения</label>
                        <input class="form-control"
                               type="text"
                               name="order"
                               id="filter-order"
                               value="{{ old('order', $filter->order) }}">
                    </div>
                </div>
            </div>

            @if (View::exists('filters.filters-spec.' . $filterShortName))
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('filters.filters-spec.' . $filterShortName)
                    </div>
                </div>
            @endif

            <div class="form-group">
                <button class="btn btn-primary">Сохранить</button>
                <a class="btn btn-link" href="{{ route('filters.index') }}">Отмена</a>
            </div>

        </form>
    </div>
@endsection
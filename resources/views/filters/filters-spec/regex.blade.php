<div class="form-group">
    <label for="regex-filter-regex" class="control-label">Регулярное выражение</label>
    <input id="regex-filter-regex"
           type="text"
           class="form-control"
           value="{{ old('filter_params.regex', $filter->filter_params->regex) }}"
           name="filter_params[regex]">
    <span class="help-block"><a target="_blank" href="http://php.net/manual/ru/book.pcre.php">Документация по регулярным выражениям</a></span>
</div>

<div class="form-group">
    <label for="set-title-filter-replace" class="control-label">Строка замены</label>
    <input class="form-control"
              name="filter_params[replace]"
              id="set-title-filter-replace"
              value="{{ old('filter_params.replace', $filter->filter_params->replace) }}">
</div>
<div class="form-group">
    <label for="filter-stop-words-words">Стоп слова</label>
    <textarea name="filter_params[words]"
              id="filter-stop-words-words"
              class="form-control">{{ $filter->filter_params->words }}</textarea>
</div>

<div class="form-group">
    <label for="filter-stop-words-type">Где искать стоп-слова</label>
    <select name="filter_params[type]" id="filter-stop-words-type" class="form-control">
        <option @if($filter->filter_params->type == 0) selected @endif value="0">В описании и заголовке медиа</option>
        <option @if($filter->filter_params->type == 1) selected @endif value="1">Только в описании</option>
        <option @if($filter->filter_params->type == 2) selected @endif value="2">Только в заголовке</option>
    </select>
</div>
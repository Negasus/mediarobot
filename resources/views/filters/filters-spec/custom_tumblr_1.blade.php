<div class="form-group">
    <label for="custom_tumblr_1_filter_title_size" class="control-label">Количество символов</label>
    <input id="custom_tumblr_1_filter_title_size"
           name="filter_params[title_size]"
           type="number"
           class="form-control"
           value="{{ old('filter_params.title_size', $filter->filter_params->title_size) }}"
           placeholder="">
    <span class="help-block">Количество символов, которое будет копировать в Заголовок из описания</span>
</div>

<div class="form-group">
    <label for="custom_tumblr_1_filter_approve_size" class="control-label">Минимальная длина заголовка для одобрения</label>
    <input id="custom_tumblr_1_filter_approve_size"
           name="filter_params[approve_size]"
           type="number"
           class="form-control"
           value="{{ old('filter_params.approve_size', $filter->filter_params->approve_size) }}"
           placeholder="">
    {{--<span class="help-block">Количество символов, которое будет копировать в Заголовок из описания</span>--}}
</div>

<div class="form-group">
    <label for="count_days" class="control-label">Количество дней</label>
    <input id="count_days"
           name="filter_params[days]"
           type="number"
           class="form-control"
           value="{{ old('filter_params.days', $filter->filter_params->days) }}"
           placeholder="">
    <span class="help-block">Количество дней. Если найден сопадающий заголовок у медиа, которое создано ранее, чем указанное количество дней назад, то статус блокировки не ставится.<br>Если 0 - то смотрим за все время</span>
</div>


<div class="form-group">
    <label for="set-title-filter-text" class="control-label">Варианты заголовков</label>
    <textarea class="form-control"
              rows="10"
              name="filter_params[text]"
              id="set-title-filter-text">{{ old('filter_params.text', $filter->filter_params->text) }}</textarea>
    <span class="help-block">Каждый вариант заголовка должен быть на отдельной строке. Заголовки для медиа будут выбираться случайным образом из этих вариантов</span>
</div>
<div class="form-group">
    <label for="set-status-filter-status" class="control-label">Статус</label>
    <select name="filter_params[status]" id="set-status-filter-status" class="form-control">
        <option @if($filter->filter_params->status == \App\Models\Media::STATUS_APPROVED) selected @endif value="{{ \App\Models\Media::STATUS_APPROVED }}">Одобрено</option>
        <option @if($filter->filter_params->status == \App\Models\Media::STATUS_BLOCKED) selected @endif value="{{ \App\Models\Media::STATUS_BLOCKED }}">Заблокировано</option>
        <option @if($filter->filter_params->status == \App\Models\Media::STATUS_PUBLISHED) selected @endif value="{{ \App\Models\Media::STATUS_PUBLISHED }}">Опубликовано</option>
    </select>
</div>
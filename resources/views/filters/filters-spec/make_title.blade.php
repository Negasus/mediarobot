<div class="form-group">
    <label for="make_title_filter_size" class="control-label">Количество символов</label>
    <input id="make_title_filter_size"
           name="filter_params[size]"
           type="number"
           class="form-control"
           value="{{ old('filter_params.size', $filter->filter_params->size) }}"
           placeholder="">
</div>

<div class="form-group">
    <label for="make_title_filter_type" class="control-label">Тип</label>
    <select name="filter_params[type]" id="make_title_filter_type" class="form-control">
        <option @if($filter->filter_params->type == 0) selected @endif value="0">Устанавливать новый заголовок, только если он пустой</option>
        <option @if($filter->filter_params->type == 1) selected @endif value="1">Устанавливать новый заголовок, только если старый меньше, чем указанное количество символов</option>
        <option @if($filter->filter_params->type == 2) selected @endif value="2">Устанавливать новый заголовок в любом случае</option>
    </select>
</div>

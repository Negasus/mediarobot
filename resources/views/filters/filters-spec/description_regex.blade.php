<div class="form-group">
    <label for="filter-description-regex-regex">Выражение</label>
    <input type="text"
           id="filter-description-regex-regex"
           class="form-control"
           name="filter_params[regex]"
           value="{{ old('filter_params.regex', $filter->filter_params->regex) }}">
</div>

<div class="form-group">
    <label for="filter-description-regex-replacement">Строка для замены</label>
    <input type="text"
           id="filter-description-regex-replacement"
           class="form-control"
           name="filter_params[replacement]"
           value="{{ old('filter_params.replacement', $filter->filter_params->replacement) }}">
    <span class="help-block">Оставьте строку пустой, чтобы удалять подходящие выражения</span>
</div>
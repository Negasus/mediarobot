<div class="form-group">
    <label class="control-label" for="cron">Расписание</label>
    <input class="form-control" type="text" id="cron" name="cron" value="{{ old('cron', $cron) }}">

    <div class="btn-group">
        <button onclick="$('#cron').val('* * * * *')" type="button" class="btn btn-default">Каждую минуту</button>
        <button onclick="$('#cron').val('*/5 * * * *')" type="button" class="btn btn-default">Каждые 5 минут</button>
        <button onclick="$('#cron').val('*/10 * * * *')" type="button" class="btn btn-default">Каждые 10 минут</button>
        <button onclick="$('#cron').val('0,30 * * * *')" type="button" class="btn btn-default">Два раза в час (в 0 и 30 мин.)</button>
        <button onclick="$('#cron').val('0 * * * *')" type="button" class="btn btn-default">Каждый час в 0 мин.</button>
    </div>
</div>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-1"><span class="badge badge-success">{{ $currentJobsCount }}</span></div>
                    <div class="col-md-11">Задач в очереди</div>
                </div>
            </div>
        </div>
    </div>
@endsection
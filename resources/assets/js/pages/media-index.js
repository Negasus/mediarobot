$(document).ready(function(){
    mediaIndex.init()
});

let mediaIndex = {

    init () {
        $("[data-fancybox]").fancybox({});
        $('#cb-check-all').change(this.cbCheckAll);
        $('.cb-item').change(this.cbCheckItem);
        $('#apply-action').click(this.applyAction)
        $('.edit-media-button').click(this.editMediaButton)
        $('#edit-media-save').click(this.editMediaSave)
    },

    // Редактирование заголовка и описания медиа
    editMediaSave () {
        $('#edit-media-save').prop('disabled', true)

        let id = $('#edit-media-id').val()
        let title = $('#edit-media-title').val()
        let description = $('#edit-media-description').val()

        $.ajax({
            url: '/media/' + id + '/update/text',
            method: 'post',
            data: {
                _token: $('meta[name=csrf-token]').attr('content'),
                title,
                description
            },
            success (r) {
                // Запрос выполнен удачно. Установим заголовок и описание в таблице
                let tr = $('tr[data-id=' + r.id + ']');
                $('.title', tr).text(r.title)
                $('.description', tr).text(r.description);
            },
            error (r) {
                console.warn(r)
                alert('Ошибка запроса')
            },
            complete () {
                $('#edit-media-modal').modal('hide')
                $('#edit-media-save').prop('disabled', false)
            }
        })
    },

    // Нажатие на кнопку редактирования загловка и описания медиа
    editMediaButton (e) {
        e.preventDefault()
        let tr = $(this).closest('tr')

        $('#edit-media-id').val($(this).attr('data-id'))
        $('#edit-media-title').val($('.title', tr).text())
        $('#edit-media-description').val($('.description', tr).text())

        $('#edit-media-modal').modal()
    },

    // Клик по кнопке "Применить к отмеченным"
    applyAction (e) {
        e.preventDefault()

        let action = $('#action').val()

        if (action == 'none') {
            return
        }

        if ($('.cb-item:checked').length == 0) {
            alert('Нет выделенных медиа')
            return
        }

        $('#form input[name="search"]').val(document.location.search.substr(1));

        $('#form').submit()
    },

    // Клик по чекбоксу в таблице
    cbCheckItem () {
        // Установим статус заглавного чекбокса
        if ($('.cb-item').length == $('.cb-item:checked').length) {
            $('#cb-check-all').prop('checked', true)
        } else {
            $('#cb-check-all').prop('checked', false)
        }
    },

    // Клик по чекбоксу "Выделить все" в шапке таблицы
    cbCheckAll () {
        let cb = $('.cb-item')

        if (cb.length == $('.cb-item:checked').length) {
            cb.prop('checked', false)
        } else {
            cb.prop('checked', true)
        }
    }

};
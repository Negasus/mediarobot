<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function guest_redirect_to_login()
    {
        $this->call('GET', '/')->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function auth_user_can_see_dashboard()
    {
        $user = factory(User::class)->make();
        auth()->login($user);

        $this->call('GET', '/')->assertStatus(200);
    }
}

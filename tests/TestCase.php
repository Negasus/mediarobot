<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * @var User $user
     */
    protected $user;

    /**
     * Make and login the user
     *
     * @return $this
     */
    protected function login() {
        $this->user = factory(User::class)->make();

        auth()->login($this->user);

        return $this;
    }
}
